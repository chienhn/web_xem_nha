package vn.dev.xem_nha.api_core;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class ApiCoreApplicationTests {

    @Test
    void contextLoads() {

        DateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date currentDate = new Date();
        //PLus 24h
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, 24);

        Date date1 = null;
        Date date2 = null;

        try {
            // calculating the difference b/w startDate and endDate
            String startDate = "12/07/2012";
            String endDate = simpleDateFormat.format(currentDate);

            date1 = simpleDateFormat.parse(startDate);
            date2 = simpleDateFormat.parse(endDate);

            long getDiff = date2.getTime() - date1.getTime();

            // using TimeUnit class from java.util.concurrent package
            long getDaysDiff = TimeUnit.MILLISECONDS.toDays(getDiff);

            System.out.println("Differance between date " + startDate + " and " + endDate + " is " + getDaysDiff + " days.");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
