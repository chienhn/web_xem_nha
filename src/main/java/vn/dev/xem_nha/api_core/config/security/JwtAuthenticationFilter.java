package vn.dev.xem_nha.api_core.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.dev.xem_nha.api_core.common.ServiceAttributes;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.exception.AccessDeniedCustomException;
import vn.dev.xem_nha.api_core.service.AuthenticateService;
import vn.dev.xem_nha.api_core.service.impl.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private final JwtTokenProvider tokenProvider;
    private final AuthenticateService authenticateService;
    private final ObjectMapper objectMapper;

    public JwtAuthenticationFilter(JwtTokenProvider tokenProvider, AuthenticateService authenticateService,@Qualifier("getObjectMapper")  ObjectMapper objectMapper) {
        this.tokenProvider = tokenProvider;
        this.authenticateService = authenticateService;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException
    {
        String requestId = (String) request.getAttribute(ServiceAttributes.REQUEST_ID);
        try {
            // Lấy jwt từ request
            String jwt = getJwtFromRequest(request);

            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                AccountDetail accountDetail =
                        authenticateService.findByAccountId(tokenProvider.getCustomerId(jwt));

                log.debug(String.format("[REQUEST_ID]: %s, authentication principal: id: %s, userName: %s",
                        requestId, accountDetail.getAccount().getId(), accountDetail.getAccount().getUserName()));

                UsernamePasswordAuthenticationToken
                        authentication = new UsernamePasswordAuthenticationToken(accountDetail, null, accountDetail.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (AccessDeniedCustomException ex) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.getWriter().println(
                    objectMapper.writeValueAsString(
                            new ServiceResponse(
                                    requestId,
                                    new Date(),
                                    HttpStatus.FORBIDDEN.value(),
                                    ex.getMessage()
                            )
                    )
            );
            return;
        } catch (Exception ex) {
        }

        filterChain.doFilter(request, response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

}

