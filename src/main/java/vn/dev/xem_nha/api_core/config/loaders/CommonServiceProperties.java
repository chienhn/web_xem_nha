package vn.dev.xem_nha.api_core.config.loaders;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ConfigurationProperties(prefix = "common-service")
@Data
public class CommonServiceProperties {
    private String baseUri;
    private String clientId;
    private String clientSecret;

    private Endpoint endpoint;

    @ConfigurationProperties(prefix = "endpoint")
    @Data
    public static class Endpoint {
        private Employee employee;

        @ConfigurationProperties(prefix = "employee")
        @Data
        public static class Employee {
            private String detail;
            private String list;
            private String changePass;
            private String userLogout;
            private String userInfo;
        }
    }

}
