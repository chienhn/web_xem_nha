package vn.dev.xem_nha.api_core.config.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.dev.xem_nha.api_core.common.ServiceAttributes;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.message.internal.ReaderWriter;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Provider
public class ClientLoggingFilter extends LoggingFeature implements ClientRequestFilter, ClientResponseFilter {
    private final Log log = LogFactory.getLog(this.getClass());
    private long begin;
    private String requestId;

    private final ObjectMapper objectMapper;

    public ClientLoggingFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext) throws IOException {
        begin = System.currentTimeMillis();
        requestId = clientRequestContext.getHeaderString(ServiceAttributes.REQUEST_ID);
        if (StringUtils.isEmpty(requestId)) {
            requestId = UUID.randomUUID().toString();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("[REQUEST_ID]: ").append(requestId);
        sb.append(" - CLIENT REQUEST ==> ");
        sb.append(" - [URI]: ").append(clientRequestContext.getUri());
        sb.append(" - [HEADER]: ").append(clientRequestContext.getHeaders());
        sb.append(" - [").append(clientRequestContext.getMethod()).append("]");
        sb.append(" - [QUERY]: ").append(clientRequestContext.getUri().getQuery());
        sb.append(" - [ENTITY]: ").append(objectMapper.writeValueAsString(clientRequestContext.getEntity()));

        log.info(sb);
    }

    @Override
    public void filter(ClientRequestContext clientRequestContext, ClientResponseContext clientResponseContext) throws IOException {
        if (clientResponseContext == null) {
            log.info("CLIENT RESPONSE ==> RESPONSE CONTEXT IS NULL !!!");
            return;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("[REQUEST_ID]: ").append(requestId);
        sb.append(" - CLIENT RESPONSE ==> ");
        sb.append(" - [IN]: ").append(System.currentTimeMillis() - begin).append(" ms");
        sb.append(" - [HEADER]: ").append(clientRequestContext.getHeaders());
        sb.append(" - [STATUS]: ").append(clientResponseContext.getStatus());
        sb.append(" - [ENTITY]: ").append(readResponseEntity(clientResponseContext));
        log.info(sb);
    }

    private String readResponseEntity(ClientResponseContext clientResponseContext) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = clientResponseContext.getEntityStream();

        final StringBuilder b = new StringBuilder();
        try {
            ReaderWriter.writeTo(in, out);

            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0) {
                b.append("\n");
            } else {
                b.append(new String(requestEntity)).append("\n");
            }
            clientResponseContext.setEntityStream(new ByteArrayInputStream(requestEntity));

        } catch (IOException ex) {
            log.error("", ex);
            return "Logging entity FAIL !";
        }
        return b.toString();
    }
}
