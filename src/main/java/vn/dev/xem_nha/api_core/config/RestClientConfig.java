package vn.dev.xem_nha.api_core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.dev.xem_nha.api_core.config.filter.ClientLoggingFilter;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Configuration
public class RestClientConfig {
    @Value(value = "${client.read_timeout}")
    private String readTimeout;

    @Value(value = "${client.connect_timeout}")
    private String connectTimeout;

    @Bean
    public Client getClient(@Qualifier("getObjectMapper") ObjectMapper objectMapper) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(new ClientLoggingFilter(objectMapper));
        clientConfig.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
        clientConfig.property(ClientProperties.READ_TIMEOUT, readTimeout);
        JacksonJaxbJsonProvider jacksonJaxbJsonProvider = new JacksonJaxbJsonProvider();
        jacksonJaxbJsonProvider.setMapper(objectMapper);
        clientConfig.register(jacksonJaxbJsonProvider);
        return ClientBuilder.newClient(clientConfig);
    }
}
