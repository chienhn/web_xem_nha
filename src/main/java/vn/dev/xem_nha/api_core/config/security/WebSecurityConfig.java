package vn.dev.xem_nha.api_core.config.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.ArrayList;
import java.util.List;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
    private static final List<String> permitAll = new ArrayList<>();

    static {
        permitAll.add("/user/login");
        permitAll.add("/swagger**");
        permitAll.add("/swagger/**");
        permitAll.add("/swagger-resources/**");
        permitAll.add("/swagger-resources");
        permitAll.add("/webjars**");
        permitAll.add("/webjars/**");
        permitAll.add("/v2**");
        permitAll.add("/v2/**");
        permitAll.add("/csrf/**");
        permitAll.add("/csrf**");
        permitAll.add("/");
//        permitAll.add("/**/**");
        permitAll.add("/poster/visitor/**");
        permitAll.add("/poster/oderByTypeProduct/**");
        permitAll.add("/poster/oderByPrice/**");
        permitAll.add("/poster/dataTypeProduct/**");
        permitAll.add("/poster/oderByAddress/**");
        permitAll.add("/bannerHome/slideShow/**");
        permitAll.add("/user/register/**");
        permitAll.add("/address/**");
        permitAll.add("/uploads/**");
    }

    public WebSecurityConfig(JwtAuthenticationFilter jwtAuthenticationFilter, CustomAuthenticationEntryPoint customAuthenticationEntryPoint) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.customAuthenticationEntryPoint = customAuthenticationEntryPoint;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(permitAll.toArray(new String[0]))
                .permitAll()
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint);

        http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
