package vn.dev.xem_nha.api_core.config.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import vn.dev.xem_nha.api_core.common.ServiceAttributes;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import vn.dev.xem_nha.api_core.service.MessageConfigService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@ControllerAdvice
@Slf4j
@Order(0)
public class LoggingResponseBodyFilter implements ResponseBodyAdvice<Object> {
    private final ObjectMapper objectMapper;
    private final HttpServletRequest httpServletRequest;

    @Autowired
    MessageConfigService messageConfigService;

    public LoggingResponseBodyFilter(
            @Qualifier("getObjectMapper") ObjectMapper objectMapper,
            HttpServletRequest httpServletRequest
    ) {
        this.objectMapper = objectMapper;
        this.httpServletRequest = httpServletRequest;

    }

    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse)
    {
        String uri = serverHttpRequest.getURI().toString();
        if (uri.contains("/uploads/")) {
            return o;
        }

        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);

        if (methodParameter.getContainingClass().isAnnotationPresent(RestController.class)) {
            if (o == null) o = new Object();

            if (o.getClass() != ServiceResponse.class) {
                o = new ServiceResponse(requestId, new Date(), messageConfigService.getServiceErrorByCode(MessageCodes.SUCCESS), o);
            }
        }

        if(SecurityContextHolder.getContext().getAuthentication() != null){
            Object context = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if(context instanceof AccountDetail){
                AccountDetail accountDetail = (AccountDetail) context;
                saveLog(accountDetail, httpServletRequest, o);
            }
        }

        String bodyResponse = objectMapper.writeValueAsString(o);
        if (bodyResponse != null) {
            if (bodyResponse.length() > 1000) {
                bodyResponse = bodyResponse.substring(0, 1000) + "...";
            }
        }
        String data = "[REQUEST_ID]: " + requestId + " - " + "[BODY RESPONSE]: " + bodyResponse;
        return o;
    }

    private void saveLog(AccountDetail accountDetail, HttpServletRequest request, Object o) {
        String ip = "";
        ip = request.getLocalAddr();
        if (request.getHeader("X-Forwarded-For") != null) {
            ip = request.getHeader("X-Forwarded-For").split(",")[0];
        }
        String path = request.getRequestURI();
//        logActionService.save(accountDetail == null? null : accountDetail.getAccount(), path, ip, (ServiceResponse) o);
    }

}
