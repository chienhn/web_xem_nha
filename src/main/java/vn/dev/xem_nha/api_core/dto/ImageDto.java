package vn.dev.xem_nha.api_core.dto;

import lombok.Data;


@Data
public class ImageDto {
    private String linkImg;
    private String nameImg;
}
