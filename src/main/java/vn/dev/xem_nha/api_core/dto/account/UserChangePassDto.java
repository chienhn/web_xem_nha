package vn.dev.xem_nha.api_core.dto.account;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ToString
public class UserChangePassDto {
    @NotNull
    private Integer userId;
    @NotBlank
    @Length(min = 5)
    private String oldPassword;
    @NotBlank
    @Length(min = 5)
    private String newPassword;
}
