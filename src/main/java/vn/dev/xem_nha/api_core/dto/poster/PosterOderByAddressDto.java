package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterOderByAddressDto {
    String provinces;
    Long total;
    Long id;
    String code;

    public PosterOderByAddressDto(String provinces, Long total, Long id, String code) {
        this.provinces = provinces;
        this.total = total;
        this.id = id;
        this.code = code;
    }
}
