package vn.dev.xem_nha.api_core.dto.bank;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@ToString
@NoArgsConstructor
public class BankDto {

    Long id;
    String nameBank;
    String numberBank;
    String name;
    String createdAt;

    public BankDto(Long id, String nameBank, String numberBank, String name, String createdAt) {
        this.id = id;
        this.nameBank = nameBank;
        this.numberBank = numberBank;
        this.name = name;
        this.createdAt = createdAt;
    }
}
