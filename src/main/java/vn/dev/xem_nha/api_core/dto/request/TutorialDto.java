package vn.dev.xem_nha.api_core.dto.request;

import vn.dev.xem_nha.api_core.dto.file.FileDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class TutorialDto {
    private Integer id;
    private String name;
    private FileDto document;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
}
