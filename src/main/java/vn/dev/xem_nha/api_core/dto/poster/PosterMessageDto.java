package vn.dev.xem_nha.api_core.dto.poster;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterMessageDto {

    Long id;
    Long posterId;
     String name;
     String email;
     String phone;
     String message;
     String createdTime;
     Integer status;
     String posterName;

    public PosterMessageDto(Long id, Long posterId, String name, String email, String phone, String message, String createdTime, Integer status, String posterName) {
        this.id = id;
        this.posterId = posterId;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.message = message;
        this.createdTime = createdTime;
        this.status = status;
        this.posterName = posterName;
    }
}
