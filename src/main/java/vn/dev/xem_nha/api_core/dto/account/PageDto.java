package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PageDto {
    private Integer id;
    private String pageName;
    private String pageUrl;
    private String pageIcon;
    private Integer parentId;

    private Integer level;


    public PageDto(Page page) {
        this.id = page.getId();
        this.pageName = page.getPageName();
        this.pageUrl = page.getPageUrl();
        this.pageIcon = page.getPageIcon();
        this.parentId = page.getParentId();
        this.level = page.getLevel();

    }
}
