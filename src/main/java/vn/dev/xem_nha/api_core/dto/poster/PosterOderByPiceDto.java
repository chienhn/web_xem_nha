package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterOderByPiceDto {
    String name;
    Long total;
    Long id;

    public PosterOderByPiceDto(String name, Long total, Long id) {
        this.name = name;
        this.total = total;
        this.id = id;
    }
}
