package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupPageRoleDto {
    private List<GroupDto> groups;
    private List<PageRoleDto> pageRoles;

    public GroupPageRoleDto(Account account, List<Page> pages, List<Role> roles) {
        List<GroupDto> groups = account.getAccountGroups()
                .stream()
                .map(AccountGroup::getGroup)
                .map(GroupDto::new)
                .collect(Collectors.toList());

        List<PageRoleDto> pageRoles =
                pages
                        .stream()
                        .filter(distinctByKey(Page::getId))
                        .map(page -> new PageRoleDto(page, roles))
                        .collect(Collectors.toList());

        this.groups = groups;
        this.pageRoles = pageRoles;
    }

    public GroupPageRoleDto(Groups groups, List<Page> pages, List<Role> roles) {
        List<GroupDto> groupDtos = new ArrayList<>();
        groupDtos.add(new GroupDto(groups));
        List<PageRoleDto> pageRoleDtos = pages
                .stream().map(pages1 -> new PageRoleDto(pages1, roles))
                .collect(Collectors.toList());
        this.groups = groupDtos;
        this.pageRoles = pageRoleDtos;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
