package vn.dev.xem_nha.api_core.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDetailDto {
    private  Integer id;
    private String fullName;
    private String avatar;
    private Integer gender;
    private String birthday;
    private String address;
    private String email;
    private String phone;
}
