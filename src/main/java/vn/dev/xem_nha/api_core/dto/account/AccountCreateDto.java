package vn.dev.xem_nha.api_core.dto.account;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class AccountCreateDto {
    //    List<Groups> groups;
//    List<PageRoleDto> pageRoles;
//    Account user;
    private String groupIdList;
    private String userName;
    private String password;
    private Integer isAdmin;

}
