package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
@NoArgsConstructor
public class ListPageRoleDto {
    private List<PageDto> pages;
    private List<RolesDto> roles;

    public ListPageRoleDto(List<Page> pages, List<Role> roles) {
        this.pages = pages.stream().map(PageDto::new).collect(Collectors.toList());
        this.roles = roles.stream().map(RolesDto::new).collect(Collectors.toList());
    }
}
