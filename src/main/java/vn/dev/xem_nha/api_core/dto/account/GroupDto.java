package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Groups;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GroupDto {
    private Integer id;
    private String groupName;
    private String description;
    private Integer status;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;

    public GroupDto(Groups groups) {
        this.id = groups.getId();
        this.groupName = groups.getGroupName();
        this.description = groups.getDescription();
        this.status = groups.getStatus();
        this.createBy = groups.getCreateBy();
        this.createTime = groups.getCreateTime();
        this.updateBy = groups.getUpdateBy();
        this.updateTime = groups.getUpdateTime();
    }
}
