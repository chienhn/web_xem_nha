package vn.dev.xem_nha.api_core.dto.banner;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class BannerHomeDto {
    private Long id;
    private String imageUrl;
    private Integer enable;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private Integer orderNo;
    private String applyDate;
    public BannerHomeDto(Long id, String imageUrl, Integer enable, String createdAt, String updatedAt, String createdBy, String updatedBy, Integer orderNo, String applyDate) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.enable = enable;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.orderNo = orderNo;
        this.applyDate = applyDate;
    }
}
