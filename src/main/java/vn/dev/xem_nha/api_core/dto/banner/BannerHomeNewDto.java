package vn.dev.xem_nha.api_core.dto.banner;

import lombok.Data;

@Data
public class BannerHomeNewDto {
    private String imageUrl;
    private Integer enable;
    private String createdBy;
    private Integer orderNo;
    private String dateApply;

    public BannerHomeNewDto(String imageUrl, Integer enable, String createdBy, Integer orderNo, String dateApply) {
        this.imageUrl = imageUrl;
        this.enable = enable;
        this.createdBy = createdBy;
        this.orderNo = orderNo;
        this.dateApply = dateApply;
    }
}
