package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.dto.file.FileDto;
import vn.dev.xem_nha.api_core.entity.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private Integer id;
    private FileDto avatar;
    private String userName;
    private String password;

    private String workPhoneNumber;
    private String personLandlineNumber;
    private String email;
    private String identificationCard;
    private String address;
    private Integer status;

    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    private Date birthday;
    private Integer gender;
    private String positionType;
    private String description;
    private Integer employeeId;
    private Long customerId;

    public AccountDto(Account account) {
        this.id = account.getId();
        this.userName = account.getUserName();

        this.status = account.getStatus().ordinal();

        this.createBy = account.getCreateBy();
        this.createTime = account.getCreateTime();
        this.updateBy = account.getUpdateBy();
        this.updateTime = account.getUpdateTime();
        if(account.getEmployee() != null){
            this.employeeId = account.getEmployee().getId();
        }
    }
}
