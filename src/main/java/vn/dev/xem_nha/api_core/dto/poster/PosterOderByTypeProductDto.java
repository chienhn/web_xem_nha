package vn.dev.xem_nha.api_core.dto.poster;

import io.swagger.models.auth.In;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterOderByTypeProductDto {
    String  typeProduct;
    Long total;
    Long id;

    public PosterOderByTypeProductDto(String typeProduct, Long total, Long id) {
        this.typeProduct = typeProduct;
        this.total = total;
        this.id = id;
    }
}
