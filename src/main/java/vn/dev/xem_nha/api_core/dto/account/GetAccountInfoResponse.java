package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.common.ServiceError;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GetAccountInfoResponse {
    private String requestId;
    private String at;
    private ServiceError error;
    private AccountDto data;
}
