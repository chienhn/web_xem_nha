package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
public class PosterManagerDto {
    Long    id;
    String  title;
    Float acreage;
    Integer numBedRooms;
    Integer numBathRooms;
    Integer numFloors;
    Long    price;
    Integer type;
    Integer typeProduct;
    Integer status;
    String  address;
    Date    postedTime;
    Long    totalView;
    Long    totalMessage;
    PosterDetailsEmployeeDto employeePDto;
    Integer district;
    Integer province;
    Integer ward;
    String yearBuilding;
    String description;
    String imageUrl;
    String direction;
    String zipUrl;
    Date duration;
}
