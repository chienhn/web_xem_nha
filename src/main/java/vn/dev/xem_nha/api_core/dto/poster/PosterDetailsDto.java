package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class PosterDetailsDto extends PosterDto {
    String description;
    String zipUrl;
    PosterDetailsEmployeeDto employeePDto;
}
