package vn.dev.xem_nha.api_core.dto;

import lombok.Data;

@Data
public class CheckRequstAnswer {
    private Integer answerId;
    private Integer clarifyId;
}
