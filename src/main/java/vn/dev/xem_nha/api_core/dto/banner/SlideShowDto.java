package vn.dev.xem_nha.api_core.dto.banner;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class SlideShowDto {
    String url;
    Integer stt;

    public SlideShowDto(String url, Integer stt) {
        this.url = url;
        this.stt = stt;
    }
}
