package vn.dev.xem_nha.api_core.dto.bank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BankAddDto {

    Long id;

    String nameBank;

    String numberBank;

    String name;
}
