package vn.dev.xem_nha.api_core.dto.file;

import lombok.Data;

@Data
public class MediaInfo {
    private String base64;
    private String fileName;
}
