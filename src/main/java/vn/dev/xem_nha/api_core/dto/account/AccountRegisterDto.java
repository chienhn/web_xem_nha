package vn.dev.xem_nha.api_core.dto.account;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class AccountRegisterDto {
    private String userName;
    private String password;
}
