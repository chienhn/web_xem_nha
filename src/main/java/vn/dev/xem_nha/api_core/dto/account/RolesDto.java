package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RolesDto {
    private Integer pageId;
    private Integer id;
    private String roleName;

    public RolesDto(Role role) {
        this.pageId = role.getPage().getId();
        this.id = role.getId();
        this.roleName = role.getRoleName();
    }
}
