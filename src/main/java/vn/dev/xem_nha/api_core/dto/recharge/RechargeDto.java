package vn.dev.xem_nha.api_core.dto.recharge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RechargeDto {

    Long id;

    String code;

    String contentRecharge;

    String member;

    Long money;

    Integer status;

    String createdAt;

    String bankName;

    String bankNumber;

    String name;

    public RechargeDto(Long id, String code, String contentRecharge, String member, Long money, Integer status, Date createdAt, String bankName, String bankNumber, String name) {
        this.id = id;
        this.code = code;
        this.contentRecharge = contentRecharge;
        this.member = member;
        this.money = money;
        this.status = status;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        this.createdAt =  createdAt == null ? null : sdf.format(createdAt);
        this.bankName = bankName;
        this.bankNumber = bankNumber;
        this.name = name;
    }
}
