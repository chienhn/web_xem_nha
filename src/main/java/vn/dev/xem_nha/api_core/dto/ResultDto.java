package vn.dev.xem_nha.api_core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class ResultDto {
    Integer resultCode; // 0: Không thành công ; 1 : Thành công
    String message; // Nội dung thông báo

    public ResultDto(Integer resultCode, String message) {
        this.resultCode = resultCode;
        this.message = message;
    }

    public interface ResultCode {
        Integer FAIL = 0;
        Integer SUCCESS = 1;
    }
}
