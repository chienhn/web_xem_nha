package vn.dev.xem_nha.api_core.dto.employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeCreateDto {
    String name;
    String avatar;
    String strBirthday;
    Integer sex;
    String address;
    String email;
    String phone;
    Integer province;
    Integer district;
    Integer ward;
}
