package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterDto {
    Long    id;
    String  title;
    Float acreage;
    Integer numBedRooms;
    Integer numBathRooms;
    Integer numFloors;
    Long    price;
    Integer type;
    Integer typeProduct;
    String yearBuilding;
    String imageUrl;
    String address;
    String postedTime;
    String direction;
    String strTypeProduct;

}
