package vn.dev.xem_nha.api_core.dto.addressDto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddressDto1 {
    Long    id;
    String  name;
    String parent_code;
    String pathType;

}
