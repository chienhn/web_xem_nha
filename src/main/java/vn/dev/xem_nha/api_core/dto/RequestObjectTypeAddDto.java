package vn.dev.xem_nha.api_core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RequestObjectTypeAddDto {
    @NotNull
    private Integer id;

    @ApiModelProperty("Danh sách id nhân sự")
    private List<Integer> accountIds;
}
