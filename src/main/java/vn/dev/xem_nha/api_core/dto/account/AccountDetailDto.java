package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDetailDto extends GroupPageRoleDto {
    private AccountDto user;

    public AccountDetailDto(Account account, List<Page> pages, List<Role> roles) {
        super(account, pages, roles);
        this.user = new AccountDto(account);
    }
}
