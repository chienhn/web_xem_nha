package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import vn.dev.xem_nha.api_core.service.impl.JwtTokenProvider;
import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ToString
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountLoginResponse extends AccountDetailDto {
    private String token;

    public AccountLoginResponse(JwtTokenProvider jwtService, Account account, List<Page> pages, List<Role> roles) {
        super(account, pages, roles);
        this.token = jwtService.generateToken(account.getId());
    }
}
