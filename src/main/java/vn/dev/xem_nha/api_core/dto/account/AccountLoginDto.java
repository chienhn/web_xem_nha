package vn.dev.xem_nha.api_core.dto.account;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class AccountLoginDto {
    @NotBlank(message = "userName must be not blank")
//    @Length(min = 5, message = "userName must have length >= 5")
    private String userName;

    @NotBlank(message = "password must be not blank")
//    @Length(min = 5, message = "password must have length >= 5")
    private String password;
}
