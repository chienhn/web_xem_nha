package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AccountShortDto {
    private Integer id;
    private String userName;


    public AccountShortDto(Account account) {
        this.id = account.getId();
        this.userName = account.getUserName();

    }
}
