package vn.dev.xem_nha.api_core.dto.bank;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BankDataSelectDto {

    Long id;
    String nameBank;
    String numberBank;
    String name;
}
