package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.AccountGroup;
import vn.dev.xem_nha.api_core.entity.account.Groups;
import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class GroupDetailDto {
    private GroupDto group;
    private List<AccountDto> users;
    private List<PageRoleDto> pageRoles;

    public GroupDetailDto(Groups groups, List<Page> pages, List<Role> roles) {
        this.group = new GroupDto(groups);
        this.users = groups
                .getAccountGroups() == null ? null : groups
                .getAccountGroups()
                .stream()
                .map(AccountGroup::getAccount)
                .map(AccountDto::new)
                .collect(Collectors.toList());

        this.pageRoles = pages == null ? null : pages
                .stream()
                .map(page -> new PageRoleDto(page, roles))
                .collect(Collectors.toList());
    }
}
