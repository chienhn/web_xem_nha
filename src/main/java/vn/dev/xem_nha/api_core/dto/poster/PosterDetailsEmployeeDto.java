package vn.dev.xem_nha.api_core.dto.poster;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PosterDetailsEmployeeDto {
    String avatar;
    String name;
    String email;
    String phone;
    String lastLogin;
    Integer isOnline;
}
