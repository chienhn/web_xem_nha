package vn.dev.xem_nha.api_core.dto.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AccountLstDto {
    private Integer id;
    private String userName;
    private String fullName;
    private Integer status;
    private String groupRole;
}
