package vn.dev.xem_nha.api_core.dto.account;

import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PageRoleDto {
    private Integer id;
    private String pageName;
    private String pageUrl;
    private String pageIcon;
    private Integer parentId;
    private Integer level;

    private String roles;



    public PageRoleDto(Page page, List<Role> roles) {
        this.id = page.getId();
        this.pageName = page.getPageName();
        this.pageUrl = page.getPageUrl();
        this.pageIcon = page.getPageIcon();
        this.parentId = page.getParentId();
        this.level = page.getLevel();


        this.roles = roles == null ? null :
                roles.stream()
                        .filter(role -> role.getPage().getId().equals(page.getId()))
                        .map(role -> String.valueOf(role.getId()))
                        .distinct()
                        .collect(Collectors.joining(","));
    }
}
