package vn.dev.xem_nha.api_core.dto.poster;

import io.swagger.models.auth.In;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
public class PostCreateDto {
    Long id;
    String title;
    String description;
    Float acreage;
    Integer totalBedrooms;
    Integer totalBathRooms;
    Integer totalFloors;
    Long price;
    String imageUrl;
    String zipUrl;
    Integer status;
    Integer typeProduct;
    Integer type;
    String yearBuilding;
    String direction;
    Integer province;
    Integer district;
    Integer ward;
    String duration;

}
