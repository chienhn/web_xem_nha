package vn.dev.xem_nha.api_core.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.Constant;
import vn.dev.xem_nha.api_core.constant.Roles;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.bank.BankAddDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDataSelectDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDto;
import vn.dev.xem_nha.api_core.entity.Bank;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.BankService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/bank")
public class BankController {

    final static String TABLE_NAME = "BANK";

    @Autowired
    ActionHistoryService actionHistoryService;
    @Autowired
    BankService bankService;
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Bank.THEM_MOI+"')")
    public ResultDto add(@RequestBody BankAddDto bankAddDto){
        Bank bank = new Bank();
        boolean checkNumberBank = checkNumberBank(bankAddDto.getNumberBank());
        if(checkNumberBank){

            return new ResultDto(ResultDto.ResultCode.FAIL, Constant.ResultMessage.Bank.THAT_BAI);
        }
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        bank.setCreatedBy(account.getUsername());
        bank.setNameBank(bankAddDto.getNameBank());
        bank.setNumberBank(bankAddDto.getNumberBank());
        bank.setName(bankAddDto.getName());
        bank.setCreatedAt(new Date());
        bankService.save(bank);
        actionHistoryService.save("Thêm mới", "Thêm mới tài khoản ngân hàng thành công", TABLE_NAME, bank.getId(), account.getUsername());
        return new ResultDto(ResultDto.ResultCode.SUCCESS, Constant.ResultMessage.Bank.THANH_CONG);


    }

    private boolean checkNumberBank(String numberBank) {
        return bankService.checkNumberBank(numberBank);
    }

    @GetMapping("/list")
    public PageDto<BankDto> getPage(@RequestParam(value = "strFromDate", required = false) String strFromDate,
                                    @RequestParam(value = "strToDate", required = false) String strToDate,
                                    @RequestParam(value = "bankName", required = false) String bankName,
                                    @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize)
    {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return bankService.getPage(fromDate, toDate, bankName, pageable);
    }
    @GetMapping("/detail")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Bank.DANH_SACH+"')")
    public Bank getDetail(@RequestParam("id") Long id){
        return bankService.getDetail(id);
    }

    @GetMapping("/delete")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Bank.XOA+"')")
    public void delete(@RequestParam("id") Long id){
         bankService.delete(id);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Bank.CAP_NHAT+"')")
    public ResultDto update(@RequestBody BankAddDto bankAddDto){
        Bank bank = bankService.getDetail(bankAddDto.getId());
        boolean checkNumberBank = checkNumberBankUpdate(bankAddDto.getNumberBank(), bankAddDto.getId());
        boolean check = checkNumberBank(bankAddDto.getNumberBank());
        if(checkNumberBank){
            AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            bank.setUpdatedBy(account.getUsername());
            bank.setNameBank(bankAddDto.getNameBank());
            bank.setNumberBank(bankAddDto.getNumberBank());
            bank.setName(bankAddDto.getName());
            bank.setUpdatedAt(new Date());
            bankService.save(bank);
            actionHistoryService.save("Cập nhật", "Cập nhật tài khoản ngân hàng thành công", TABLE_NAME, bank.getId(), account.getUsername());
            return new ResultDto(ResultDto.ResultCode.SUCCESS, Constant.ResultMessage.Bank.CAP_NHAT);
        }
        else if(!check){
            AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            bank.setUpdatedBy(account.getUsername());
            bank.setNameBank(bankAddDto.getNameBank());
            bank.setNumberBank(bankAddDto.getNumberBank());
            bank.setName(bankAddDto.getName());
            bank.setUpdatedAt(new Date());
            bankService.save(bank);
            actionHistoryService.save("Cập nhật", "Cập nhật tài khoản ngân hàng thành công", TABLE_NAME, bank.getId(), account.getUsername());
            return new ResultDto(ResultDto.ResultCode.SUCCESS, Constant.ResultMessage.Bank.CAP_NHAT);
        }
        return new ResultDto(ResultDto.ResultCode.FAIL, Constant.ResultMessage.Bank.THAT_BAI);

    }

    private boolean checkNumberBankUpdate(String numberBank, Long id) {
        return bankService.checkNumberBankUpdate(numberBank, id);
    }

    @GetMapping(value = "/dataSelect")
    public List<BankDataSelectDto> dataSelectAll(@RequestParam(value = "id", required = false) Long id) {
        return bankService.dataSelectAll(id);
    }



}
