package vn.dev.xem_nha.api_core.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/uploads")
public class GetFileController {

    @Value(value = "${upload.path-img}")
    private String uploadPathImg;

    @Value(value = "${upload.path-zip}")
    private String uploadPathZip;

    @GetMapping(value = "/image/{name}")
    public ResponseEntity<byte[]> imgFile(@PathVariable("name") String name) throws IOException {

        InputStream inputStream = new FileInputStream(uploadPathImg + "/" + name);

        byte[] imageBytes = StreamUtils.copyToByteArray(inputStream);

        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(imageBytes);
    }

    @GetMapping(value = "/zip/{name}", produces="application/zip")
    public InputStreamResource zipFile(@PathVariable("name") String name, HttpServletResponse response) throws IOException {

        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename= " + name);

        File file = new File(uploadPathZip + name);
        if (file.exists()) {
            return new InputStreamResource(new FileInputStream(uploadPathZip + name));
        } else {
            return null;
        }
    }
}
