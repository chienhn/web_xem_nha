package vn.dev.xem_nha.api_core.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeCreateDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDetailDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDto;
import vn.dev.xem_nha.api_core.dto.file.FileDto;
import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.Employee;
import vn.dev.xem_nha.api_core.service.AccountService;
import vn.dev.xem_nha.api_core.service.EmployeeService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Value(value = "${upload.path-img}")
    private String uploadPathImg;

    @Value(value = "${upload.base-uri}")
    private String baseUri;

    private final EmployeeService employeeService;

    @Autowired AccountService accountService;

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/list")
    @ApiOperation("Danh sách nhân sự")
//    @RolesAllowed({ Roles.System.GroupAccount.VIEW_LIST})
    public PageDto<EmployeeDto> getList(@RequestParam(value = "strFromDate", required = false) String strFromDate,
                                        @RequestParam(value = "gender", required = false) Integer gender,
                                        @RequestParam(value = "name", required = false) String name,
                                        @RequestParam(value = "strToDate", required = false) String strToDate,
                                        @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return employeeService.getList(fromDate, toDate, gender, name, pageable);
    }

    @GetMapping("/detail")
    @ApiOperation("Chi tiết nhân sự")
    public EmployeeDetailDto getDetail(@RequestParam("id") Integer id) {
        return employeeService.getDetail(id);
    }
    @GetMapping(value = "/infoThisCustomer")
    public Employee infoThisCustomer(){
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountDetail.getAccount();
        return employeeService.find(account.getEmployee().getId());
    }
    @GetMapping("/checkExists")
    public boolean checkExists(){
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = accountDetail.getAccount().getEmployee();
        return employee != null;
    }
    @PostMapping("/uploadImage")
    public FileDto uploadImg(@RequestParam("image") MultipartFile dataFile){
        log.info("dataFile: {}", dataFile);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        String name = "Image_" + dtf.format(now) + "_name_" + dataFile.getOriginalFilename();
        Path pathFile = Paths.get(uploadPathImg + "/" + name);

        String url = baseUri + "uploads/image/" + name;
        FileDto fileDto = new FileDto(url, name);
        try {
            Files.copy(dataFile.getInputStream(), pathFile);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }

        return fileDto;
    }

    @PostMapping("/add")
    public void add(@RequestBody EmployeeCreateDto employeeCreateDto){
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        log.info("employeeCreateDto: {}", employeeCreateDto);
        Employee employee = new Employee();
        employee.setFullName(employeeCreateDto.getName());
        employee.setAvatar(employeeCreateDto.getAvatar());
        employee.setSex(employeeCreateDto.getSex());
        employee.setApprovalStatus(1);
        employee.setProvince(employeeCreateDto.getProvince());
        employee.setDistrict(employeeCreateDto.getDistrict());
        employee.setWard(employeeCreateDto.getWard());
        employee.setAddress(employeeCreateDto.getAddress());
        employee.setWorkPhoneNumber(employeeCreateDto.getPhone());
        employee.setEmail(employeeCreateDto.getEmail());
        employee.setIsOnline(1);
        Date birthday;
        try {
            if (employeeCreateDto.getStrBirthday() != null) {
                birthday = dateFormat.parse(employeeCreateDto.getStrBirthday());
                employee.setBirthday(birthday);
            }
        } catch (Exception e) {
        }

        employeeService.save(employee);
        Account account = accountDetail.getAccount();
        account.setEmployee(employee);
        accountService.createOrUpdate(account);

    }

    @PostMapping("/update")
    public void update(@RequestBody EmployeeCreateDto employeeCreateDto){
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        log.info("employeeCreateDto: {}", employeeCreateDto);
        Employee employee = employeeService.getById(accountDetail.getAccount().getEmployee().getId());
        employee.setFullName(employeeCreateDto.getName());
        employee.setAvatar(employeeCreateDto.getAvatar());
        employee.setSex(employeeCreateDto.getSex());
        employee.setProvince(employeeCreateDto.getProvince());
        employee.setDistrict(employeeCreateDto.getDistrict());
        employee.setWard(employeeCreateDto.getWard());
        employee.setAddress(employeeCreateDto.getAddress());
        employee.setWorkPhoneNumber(employeeCreateDto.getPhone());
        employee.setEmail(employeeCreateDto.getEmail());

        Date birthday;
        try {
            if (employeeCreateDto.getStrBirthday() != null) {
                birthday = dateFormat.parse(employeeCreateDto.getStrBirthday());
                employee.setBirthday(birthday);
            }
        } catch (Exception e) {
        }

        employeeService.save(employee);

    }
}
