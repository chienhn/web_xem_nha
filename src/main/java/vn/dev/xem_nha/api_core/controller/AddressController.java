package vn.dev.xem_nha.api_core.controller;

import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto;
import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto1;
import vn.dev.xem_nha.api_core.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/address")
public class AddressController {

    @Resource AddressService addressService;

    @GetMapping(value = "/dataProvinces")
    public List<AddressDto> dataCity() {
        return addressService.getDataCity();
    }

    @GetMapping(value = "/dataDistricts")
    public List<AddressDto> dataDistricts(@RequestParam("parentCode") String parentCode) {
        return addressService.dataSelectOfProvinces(parentCode);
    }

    @GetMapping(value = "/dataWards")
    public List<AddressDto1> dataWards(@RequestParam("parentCode") String parentCode) {
        return addressService.dataSelectOfWards(parentCode);
    }
}
