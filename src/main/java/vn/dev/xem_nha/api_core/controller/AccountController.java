package vn.dev.xem_nha.api_core.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.AccountStatus;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import vn.dev.xem_nha.api_core.constant.Roles;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.account.*;
import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.Employee;
import vn.dev.xem_nha.api_core.service.AccountService;
import vn.dev.xem_nha.api_core.service.EmployeeService;
import vn.dev.xem_nha.api_core.service.MessageConfigService;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
//@Api(value = "API Tài khoản", authorizations = @Authorization(value = "apiKey"))
@RestController
@RequestMapping("/user")
public class AccountController {

    private final AccountService _accountService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    MessageConfigService messageConfigService;

    public AccountController( AccountService _accountService) {
        this._accountService = _accountService;
    }

    @PostMapping("/login")
    public AccountLoginResponse login(@Valid @NotNull @RequestBody AccountLoginDto userLoginDto) {
        return _accountService.login(userLoginDto);
    }

    @GetMapping("/getPageRole")
    public GroupPageRoleDto getRole(@ApiParam(value = "Bộ lọc tìm kiếm theo loại menu") @RequestParam(value = "menuId", required = false) Integer menuId,
                                    @AuthenticationPrincipal AccountDetail accountDetail
    ) {
        return _accountService.getPageRoles(accountDetail.getAccount(), menuId);
    }

    @GetMapping("/delete")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.XOA_NGUOI_DUNG +"')")
    public ResultDto deleteAcc(@NotNull @RequestParam("userId") Integer userId) {
        Account account = _accountService.findFirstById(userId);
        ResultDto result = new ResultDto();
        if (Objects.equals(account.getUserName(), "admin")){
            result.setResultCode(ResultDto.ResultCode.FAIL);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.MASTER_ACCOUNT_CANNOT_DELETE));
        }
        else {
            if(account.getEmployee()!=null){
                account.getEmployee().setApprovalStatus(0);
            }
            account.setStatus(AccountStatus.BLOCKED);
            result.setResultCode(ResultDto.ResultCode.SUCCESS);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.DELETE_ACCOUNT_SUCCESS));
            _accountService.updateAccount(account);
        }

        return result;
    }

    @GetMapping("list")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.DANH_SACH_NGUOI_DUNG +"')")
    public PageDto<AccountLstDto> getList(@RequestParam(value = "userId", required = false) Integer userId,
                                       @RequestParam("pageIndex") Integer pageIndex,
                                       @RequestParam("pageSize") Integer pageSize,
                                       @RequestParam(value = "status", required = false) Integer status

    ) {
        return _accountService.getList(userId, pageIndex, pageSize, status);
    }

    @PostMapping("/updateGroupForAccount")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.CAP_NHAT_NGUOI_DUNG +"')")
    public Map<String, Object> updateGroupForAccount1(
            @RequestParam("accountId") Integer accountId,
            @RequestParam("groupIdList") String groupIdList,
            @RequestParam(value = "status", required = false) Integer status
    ) {
        return _accountService.updateGroupForAccount(accountId, groupIdList, status);
    }

    @GetMapping("detail")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.CHI_TIET_NHOM_NGUOI_DUNG +"')")
    public AccountDetailDto getDetail(@NotNull @RequestParam("userId") Integer userId) {
        return _accountService.getDetail(userId);
    }

    @GetMapping("info")
    public AccountDetailDto getInfo(@AuthenticationPrincipal AccountDetail accountDetail) {
        return _accountService.getDetail(accountDetail.getAccount().getId());
    }

    @GetMapping("page/list")
    @PermitAll
    public ListPageRoleDto getListPages() {
        return _accountService.getListPages();
    }


    @GetMapping("group/list")
    public List<GroupDto> getListGroups() {
        log.info("account username: {}", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return _accountService.getListGroups();
    }

    @GetMapping("/group/ofSystem")
    public List<GroupDto> getListGroupsSystem() {

        return _accountService.getListGroups();
    }

    @GetMapping("group/detail")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.CHI_TIET_NHOM_NGUOI_DUNG +"')")
    public GroupDetailDto getDetailGroup(@NotNull @RequestParam("groupId") Integer groupId) {
        return _accountService.getDetailGroup(groupId);
    }

    @PostMapping("group/add")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.THEM_MOI_NHOM_NGUOI_DUNG +"')")
    public void addGroup(@NotNull @Valid @RequestBody GroupDetailDto groupDetailDto) {
        _accountService.addGroup(groupDetailDto);
    }

    @PostMapping("group/update")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.CAP_NHAT_NHOM_NGUOI_DUNG +"')")
    public void updateGroup(@NotNull @Valid @RequestBody GroupDetailDto groupDetailDto) {
        _accountService.updateGroup(groupDetailDto);
    }

    @GetMapping("group/delete")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.XOA_NHOM_NGUOI_DUNG +"')")
    public void deleteGroup(@NotNull @RequestParam("groupId") Integer groupId) {
        _accountService.deleteGroup(groupId);
    }

    @PostMapping("group/addUser")
//    @RolesAllowed({ Roles.System.GroupAccount.ADD})
    public void groupAddUser(@NotNull @Valid @RequestBody GroupDetailDto groupDetailDto) {
        _accountService.addUserToGroup(groupDetailDto);
    }

    @GetMapping("logout")
//    @PreAuthorize("hasRole(@roleService.accountView())")
    public void logout() {

        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeService.getById(accountDetail.getAccount().getEmployee().getId());
        log.info("employee: {}", employee.getId());

        employee.setIsOnline(0);
        employee.setLastLogin(new Date());
        employeeService.save(employee);

    }

    @PostMapping("changePass")
//    @RolesAllowed(Roles.System.Account.UPDATE)
    public void changePass(@NotNull @Valid @RequestBody UserChangePassDto userChangePassDto) {
        _accountService.changePassword(userChangePassDto);
    }

    @GetMapping("resetPass")
    public ResultDto resetPass(@NotNull @RequestParam("userId") Integer userId) {
        ResultDto result = _accountService.resetPassword(userId);
        return result;
    }

    @PostMapping("/addByManager")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Account.THEM_MOI_NGUOI_DUNG +"')")
    public Map<String, Object> addNewAccount(@RequestBody AccountCreateDto accountCreateDto) {
        return _accountService.addByManager(accountCreateDto);
    }
    @PostMapping("/changePasswordThisAccount")
    public ResultDto changePasswordThisAccount(@RequestBody Map<String, String> mapParamRequest) {
        return _accountService.changePassThisAccount(mapParamRequest.get("passwordOld"), mapParamRequest.get("passwordNew"));
    }

    @PostMapping("/register")
    public ResultDto register(@RequestBody AccountRegisterDto accountCreateDto) {
        return _accountService.register(accountCreateDto);
    }

}
