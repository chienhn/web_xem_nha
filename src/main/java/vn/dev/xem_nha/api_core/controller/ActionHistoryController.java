package vn.dev.xem_nha.api_core.controller;

import vn.dev.xem_nha.api_core.entity.ActionHistory;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/ActionHistory")
public class ActionHistoryController {
    @Autowired ActionHistoryService actionHistoryService;

    // list action history
    @GetMapping("/listHistory")
    public List<ActionHistory> list(@Param("tableName") String tableName, Long tableId){
        return actionHistoryService.listAllAction(tableName, tableId);
    }

    //Comment
    @PostMapping("/comment")
    public void saveComment (@RequestBody ActionHistory actionHistory){
        actionHistoryService.save("Bình luận", actionHistory.getContent(), actionHistory.getTableName(), actionHistory.getTableId(),actionHistory.getCreatedBy());
    }
}
