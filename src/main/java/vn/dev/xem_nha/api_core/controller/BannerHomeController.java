package vn.dev.xem_nha.api_core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.Constant;
import vn.dev.xem_nha.api_core.constant.Roles;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeNewDto;
import vn.dev.xem_nha.api_core.dto.banner.SlideShowDto;
import vn.dev.xem_nha.api_core.dto.file.FileDto;
import vn.dev.xem_nha.api_core.entity.BannerHome;
import vn.dev.xem_nha.api_core.entity.Poster;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.BannerHomeService;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/bannerHome")
public class BannerHomeController {

    @Value(value = "${upload.path-img}")
    private String uploadPathImg;
    @Value(value = "${upload.base-uri}")
    private String baseUri;
    @Resource BannerHomeService bannerHomeService;
    @Autowired ActionHistoryService actionHistoryService;
    final static String TABLE_NAME = "BANNER";
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @GetMapping("/list")
    public PageDto<BannerHomeDto> getPage(@RequestParam(value = "strFromDate", required = false) String strFromDate,
                                          @RequestParam(value = "strToDate", required = false) String strToDate,
                                          @RequestParam(value = "enable", required = false) Integer enable,
                                          @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize)
    {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return bannerHomeService.getPage(fromDate, toDate, enable, pageable);
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Banner.THEM_MOI +"')")
    public ResultDto add(@NotNull @RequestBody BannerHomeNewDto bannerHomeNewDto){

        return bannerHomeService.createBanner(bannerHomeNewDto);

    }

    @PostMapping("/uploadImage")
    public FileDto uploadImg(@RequestParam("image") MultipartFile dataFile){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        String name = "Image_" + dtf.format(now) + "_name_" + dataFile.getOriginalFilename();
        Path pathFile = Paths.get(uploadPathImg + "/" + name);

        String url = baseUri + "uploads/image/" + name;
        FileDto fileDto = new FileDto(url, name);
        try {
            Files.copy(dataFile.getInputStream(), pathFile);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }

        return fileDto;
    }

    @GetMapping("/slideShow")
    public List<SlideShowDto> slideShow() {
        return bannerHomeService.slideshow();
    }

    @GetMapping(value = "/detail")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Banner.CHI_TIET +"')")
    public BannerHomeDto getDetail(@RequestParam(value ="id", required = false) Long id){
        return bannerHomeService.findBanner(id);
    }

}
