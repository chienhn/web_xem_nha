package vn.dev.xem_nha.api_core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.Roles;
import vn.dev.xem_nha.api_core.dto.ImageDto;
import vn.dev.xem_nha.api_core.dto.recharge.RechargeDto;
import vn.dev.xem_nha.api_core.entity.Bank;
import vn.dev.xem_nha.api_core.entity.Poster;
import vn.dev.xem_nha.api_core.entity.Recharge;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.RechargeService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/recharge")
public class RechargeController {

    final static String TABLE_NAME = "RECHARGE";

    @Autowired
    ActionHistoryService actionHistoryService;



    @Autowired
    RechargeService rechargeService;

    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Recharge.THANH_VIEN_NAP_TIEN +"')")
    public void add(@RequestBody Map<String, Object> mapParams){
        rechargeService.add(mapParams);
    }

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority({'"+ Roles.System.Recharge.DANH_SACH +"'})")
    public PageDto<RechargeDto> getListOrder(
            @RequestParam(value ="strFromDate", required = false) String strFromDate,
            @RequestParam(value ="strToDate", required = false) String strToDate,
            @RequestParam(value = "status", required = false) Integer status,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "pageSize", required = false) Integer pageSize
    ) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        PageDto<RechargeDto> pageDto = rechargeService.getPage(fromDate,  toDate, status,accountDetail.getAccount().getEmployee().getId().longValue(), pageable);

        return pageDto;
    }

    @GetMapping("/listMana")
    @PreAuthorize("hasAnyAuthority({'"+ Roles.System.Recharge.DANH_SACH_MANAGER +"'})")
    public PageDto<RechargeDto> getList(
            @RequestParam(value ="strFromDate", required = false) String strFromDate,
            @RequestParam(value ="strToDate", required = false) String strToDate,
            @RequestParam(value = "status", required = false) Integer status,
            @RequestParam(value = "pageIndex", required = false) Integer pageIndex,
            @RequestParam(value = "pageSize", required = false) Integer pageSize
    ) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        PageDto<RechargeDto> pageDto = rechargeService.getPageMana(fromDate,  toDate, status, pageable);

        return pageDto;
    }
    @GetMapping("/detail")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Recharge.CHI_TIET+"','"+ Roles.System.Recharge.ADMIN_CHI_TIET+"')")
    public Recharge getDetail(@RequestParam("id") Long id){
        return rechargeService.getDetail(id);
    }

    @PostMapping("/approve")
    @PreAuthorize("hasAnyAuthority('"+ Roles.System.Recharge.PHE_DUYET+"')")
    public void accept(@RequestBody Recharge recharge) throws ParseException {

        rechargeService.approve(recharge);

    }

    @GetMapping("/surplus")
    public Long getSurplus(){

        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return rechargeService.getSurplus(Long.valueOf(accountDetail.getAccount().getEmployee().getId()));
    }
}
