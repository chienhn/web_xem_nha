package vn.dev.xem_nha.api_core.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.Constant;
import vn.dev.xem_nha.api_core.constant.Roles;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.file.FileDto;
import vn.dev.xem_nha.api_core.dto.poster.*;
import vn.dev.xem_nha.api_core.entity.Poster;
import vn.dev.xem_nha.api_core.entity.PosterMessage;
import vn.dev.xem_nha.api_core.entity.Recharge;
import vn.dev.xem_nha.api_core.entity.account.Employee;
import vn.dev.xem_nha.api_core.repository.EmployeeRepo;
import vn.dev.xem_nha.api_core.service.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/poster")
public class PostController {

    @Value(value = "${upload.path-img}")
    private String uploadPathImg;

    @Value(value = "${upload.path-zip}")
    private String uploadPathZip;

    @Value(value = "${upload.base-uri}")
    private String baseUri;

    @Autowired
    PosterService posterService;
    @Autowired
    ActionHistoryService actionHistoryService;
    @Autowired
    PosterMessageService posterMessageService;
    @Autowired
    AccountService accountService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    EmployeeRepo employeeRepo;

    final static String TABLE_NAME = "POSTER";

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");


    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.THEM_MOI_MEMBER + "')")
    public ResultDto add(@RequestBody PostCreateDto posterForm) throws ParseException {
        Poster poster = new Poster();
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        poster.setEmployeeId(account.getAccount().getEmployee().getId().longValue());
        poster.setTitle(posterForm.getTitle());
        poster.setDescription(posterForm.getDescription());
        poster.setAcreage(posterForm.getAcreage());
        poster.setTotalBedrooms(posterForm.getTotalBedrooms());
        poster.setTotalBathRooms(posterForm.getTotalBathRooms());
        poster.setTotalFloors(posterForm.getTotalFloors());
        poster.setPrice(posterForm.getPrice());
        poster.setImageUrl(posterForm.getImageUrl());
        poster.setZipUrl(posterForm.getZipUrl());
        poster.setStatus(posterForm.getStatus());
        poster.setTypeProduct(posterForm.getTypeProduct());
        poster.setType(posterForm.getType());
        poster.setYearBuilding(posterForm.getYearBuilding());
        poster.setDirection(posterForm.getDirection());
        poster.setProvince(posterForm.getProvince());
        poster.setDistrict(posterForm.getDistrict());
        poster.setWard(posterForm.getWard());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateApply = null;
        //Date toDate = null;
        try {
            dateApply = dateFormat.parse(posterForm.getDuration());

        } catch (Exception e) {
        }
        poster.setDuration(dateApply);

        if (posterForm.getPrice() <= 1000000000 && posterForm.getPrice() > 0) {
            poster.setLevel(1);
        } else if (posterForm.getPrice() > 1000000000 && posterForm.getPrice() <= 2000000000) {
            poster.setLevel(2);
        } else if (posterForm.getPrice() > 2000000000 && posterForm.getPrice() <= 3000000000L) {
            poster.setLevel(3);
        } else if (posterForm.getPrice() > 3000000000L && posterForm.getPrice() <= 5000000000L) {
            poster.setLevel(4);
        } else if (posterForm.getPrice() > 5000000000L && posterForm.getPrice() <= 7000000000L) {
            poster.setLevel(5);
        } else if (posterForm.getPrice() > 7000000000L && posterForm.getPrice() <= 10000000000L) {
            poster.setLevel(6);
        } else if (posterForm.getPrice() > 10000000000L && posterForm.getPrice() <= 20000000000L) {
            poster.setLevel(7);
        } else if (posterForm.getPrice() > 20000000000L && posterForm.getPrice() <= 30000000000L) {
            poster.setLevel(8);
        } else if (posterForm.getPrice() > 30000000000L && posterForm.getPrice() <= 50000000000L) {
            poster.setLevel(9);
        } else if (posterForm.getPrice() > 50000000000L && posterForm.getPrice() <= 100000000000L) {
            poster.setLevel(10);
        } else if (posterForm.getPrice() > 100000000000L) {
            poster.setLevel(11);
        }

        if (posterForm.getAcreage() <= 30) {
            poster.setLevelArea(1);
        } else if (posterForm.getAcreage() > 30 && posterForm.getAcreage() <= 50) {
            poster.setLevelArea(2);
        } else if (posterForm.getAcreage() > 50 && posterForm.getAcreage() <= 70) {
            poster.setLevelArea(3);
        } else if (posterForm.getAcreage() > 70 && posterForm.getAcreage() <= 100) {
            poster.setLevelArea(4);
        } else if (posterForm.getAcreage() > 100 && posterForm.getAcreage() <= 150) {
            poster.setLevelArea(5);
        } else if (posterForm.getAcreage() > 150 && posterForm.getAcreage() <= 300) {
            poster.setLevelArea(6);
        } else if (posterForm.getAcreage() > 300 && posterForm.getAcreage() <= 500) {
            poster.setLevelArea(7);
        } else if (posterForm.getAcreage() >= 500) {
            poster.setLevelArea(8);
        }
        posterService.save(poster);
        employeeService.save(account.getAccount().getEmployee());
        actionHistoryService.save("Thêm mới ", "Thêm mới Bất động sản thành công", TABLE_NAME, poster.getId(), account.getUsername());
        return new ResultDto(ResultDto.ResultCode.SUCCESS, "Thêm mới thành công");


    }

    @PostMapping("/update")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.CAP_NHAT_MEMBER + "')")
    public void update(@RequestBody PostCreateDto posterForm) {
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Poster poster = posterService.getById(posterForm.getId());
        poster.setUpdateBy(account.getUsername());
        poster.setStatus(1);
        if (posterForm.getTitle() != null) poster.setTitle(posterForm.getTitle());
        if (posterForm.getDescription() != null) poster.setDescription(posterForm.getDescription());
        if (posterForm.getAcreage() != null) poster.setAcreage(posterForm.getAcreage());
        if (posterForm.getTotalBathRooms() != null) poster.setTotalBathRooms(posterForm.getTotalBathRooms());
        if (posterForm.getTotalBedrooms() != null) poster.setTotalBedrooms(posterForm.getTotalBedrooms());
        if (posterForm.getTotalFloors() != null) poster.setTotalFloors(posterForm.getTotalFloors());
        if (posterForm.getPrice() != null) poster.setPrice(posterForm.getPrice());
        if (posterForm.getImageUrl() != null) poster.setImageUrl(posterForm.getImageUrl());
        if (posterForm.getZipUrl() != null) poster.setZipUrl(posterForm.getZipUrl());
        if (posterForm.getType() != null) poster.setType(posterForm.getType());
        if (posterForm.getProvince() != null) poster.setProvince(posterForm.getProvince());
        if (posterForm.getDistrict() != null) poster.setDistrict(posterForm.getDistrict());
        if (posterForm.getWard() != null) poster.setWard(posterForm.getWard());
        if (posterForm.getTypeProduct() != null) poster.setTypeProduct(posterForm.getTypeProduct());
        if (posterForm.getDirection() != null) poster.setDirection(posterForm.getDirection());
        if (posterForm.getYearBuilding() != null) poster.setYearBuilding(posterForm.getYearBuilding());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date dateApply = null;
        //Date toDate = null;
        try {
            dateApply = dateFormat.parse(posterForm.getDuration());

        } catch (Exception e) {
        }
        if (posterForm.getDuration() != null) poster.setDuration(dateApply);

        if (posterForm.getPrice() <= 1000000000 && posterForm.getPrice() > 0) {
            poster.setLevel(1);
        } else if (posterForm.getPrice() > 1000000000 && posterForm.getPrice() <= 2000000000) {
            poster.setLevel(2);
        } else if (posterForm.getPrice() > 2000000000 && posterForm.getPrice() <= 3000000000L) {
            poster.setLevel(3);
        } else if (posterForm.getPrice() > 3000000000L && posterForm.getPrice() <= 5000000000L) {
            poster.setLevel(4);
        } else if (posterForm.getPrice() > 5000000000L && posterForm.getPrice() <= 7000000000L) {
            poster.setLevel(5);
        } else if (posterForm.getPrice() > 7000000000L && posterForm.getPrice() <= 10000000000L) {
            poster.setLevel(6);
        } else if (posterForm.getPrice() > 10000000000L && posterForm.getPrice() <= 20000000000L) {
            poster.setLevel(7);
        } else if (posterForm.getPrice() > 20000000000L && posterForm.getPrice() <= 30000000000L) {
            poster.setLevel(8);
        } else if (posterForm.getPrice() > 30000000000L && posterForm.getPrice() <= 50000000000L) {
            poster.setLevel(9);
        } else if (posterForm.getPrice() > 50000000000L && posterForm.getPrice() <= 100000000000L) {
            poster.setLevel(10);
        } else if (posterForm.getPrice() > 100000000000L) {
            poster.setLevel(11);
        }

        if (posterForm.getAcreage() <= 30) {
            poster.setLevelArea(1);
        } else if (posterForm.getAcreage() > 30 && posterForm.getAcreage() <= 50) {
            poster.setLevelArea(2);
        } else if (posterForm.getAcreage() > 50 && posterForm.getAcreage() <= 70) {
            poster.setLevelArea(3);
        } else if (posterForm.getAcreage() > 70 && posterForm.getAcreage() <= 100) {
            poster.setLevelArea(4);
        } else if (posterForm.getAcreage() > 100 && posterForm.getAcreage() <= 150) {
            poster.setLevelArea(5);
        } else if (posterForm.getAcreage() > 150 && posterForm.getAcreage() <= 300) {
            poster.setLevelArea(6);
        } else if (posterForm.getAcreage() > 300 && posterForm.getAcreage() <= 500) {
            poster.setLevelArea(7);
        } else if (posterForm.getAcreage() >= 500) {
            poster.setLevelArea(8);
        }
        posterService.save(poster);
        actionHistoryService.save("Cập nhật ", "Cập nhật Bất động sản thành công", TABLE_NAME, poster.getId(), account.getUsername());
    }

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.LIST + "')")
    public PageDto<PosterManagerDto> getPageMana(@RequestParam(value = "strFromDate", required = false) String strFromDate,
                                                 @RequestParam(value = "strToDate", required = false) String strToDate,
                                                 @RequestParam(value = "type", required = false) Integer type,
                                                 @RequestParam(value = "typeProduct", required = false) Integer typeProduct,
                                                 @RequestParam(value = "status", required = false) Integer status,
                                                 @RequestParam(value = "province", required = false) Integer province,
                                                 @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return posterService.getPagePosterMana(fromDate, toDate, status, type, typeProduct, province, pageable);
    }

    @GetMapping("/listMember")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.LIST_MEMBER + "')")
    public PageDto<PosterManagerDto> getPageManaMember(@RequestParam(value = "strFromDate", required = false) String strFromDate,
                                                       @RequestParam(value = "strToDate", required = false) String strToDate,
                                                       @RequestParam(value = "type", required = false) Integer type,
                                                       @RequestParam(value = "typeProduct", required = false) Integer typeProduct,
                                                       @RequestParam(value = "status", required = false) Integer status,
                                                       @RequestParam(value = "province", required = false) Integer province,
                                                       @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return posterService.getPagePosterManaMember(fromDate, toDate, status, type, typeProduct, province, accountDetail.getAccount().getEmployee().getId().longValue(), pageable);
    }

    @GetMapping("/messageAll")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.TIN_NHAN + "')")
    public PageDto<PosterMessageDto> listMessageAll(@RequestParam(value = "id", required = false) Long id,
                                                    @RequestParam(value = "strFromDate", required = false) String strFromDate,
                                                    @RequestParam(value = "strToDate", required = false) String strToDate,
                                                    @RequestParam(value = "status", required = false) Integer status,
                                                    @RequestParam(value = "posterId", required = false) Long posterId,
                                                    @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return posterMessageService.listMessageAll(id, fromDate, toDate, status, posterId, pageable);
    }

    @GetMapping("/messageAllMember")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.TIN_NHAN_MEMBER + "')")
    public PageDto<PosterMessageDto> listMessageAllMember(@RequestParam(value = "id", required = false) Long id,
                                                          @RequestParam(value = "strFromDate", required = false) String strFromDate,
                                                          @RequestParam(value = "strToDate", required = false) String strToDate,
                                                          @RequestParam(value = "status", required = false) Integer status,
                                                          @RequestParam(value = "posterId", required = false) Long posterId,
                                                          @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                                          @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        Date fromDate = null;
        try {
            if (strFromDate != null) {
                strFromDate = strFromDate + " 00:00:00";
                fromDate = dateFormat.parse(strFromDate);
            }
        } catch (Exception e) {
        }

        Date toDate = null;
        try {
            if (strToDate != null) {
                strToDate = strToDate + " 23:59:59";
                toDate = dateFormat.parse(strToDate);
            }
        } catch (Exception e) {
        }


        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return posterMessageService.listMessageAllMember(id, fromDate, toDate, status, posterId, accountDetail.getAccount().getEmployee().getId().longValue(), pageable);
    }


    @GetMapping("/message/detail")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.CHI_TIET_TIN_NHAN + "','" + Roles.System.Post.CHI_TIET_TIN_NHAN_MEMBER + "')")
    public PosterMessageDto detail(@RequestParam("messageId") Long id) {

        return posterMessageService.get(id);
    }

    @PostMapping("/message/seen")
    public void seen(@RequestBody PosterMessage posterMessage) {
        PosterMessage poster = posterMessageService.getPosterMessageById(posterMessage.getId());
        poster.setStatus(posterMessage.getStatus());
        posterMessageService.save(poster);
    }

    @PostMapping("/uploadImage")
    public List<FileDto> uploadImg(@RequestParam("image") MultipartFile[] dataFiles) {

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        List<FileDto> listFile = new ArrayList<>();
        for (MultipartFile dataFile : dataFiles) {
            String name = "Image_" + dtf.format(now) + "_name_" + dataFile.getOriginalFilename();
            Path pathFile = Paths.get(uploadPathImg + "/" + name);

            String url = baseUri + "uploads/image/" + name;
            FileDto fileDto = new FileDto(url, name);
            listFile.add(fileDto);
            try {
                Files.copy(dataFile.getInputStream(), pathFile);
            } catch (Exception e) {
                throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
            }
        }

        return listFile;
    }


    @PostMapping("/uploadZip")
    public FileDto uploadZip(@RequestParam("zip") MultipartFile dataFile) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        String name = "Zip_" + dtf.format(now) + "_name_" + dataFile.getOriginalFilename();
        Path pathFile = Paths.get(uploadPathZip + name);

        String url = baseUri + "uploads/zip/" + name;
        try {
            Files.copy(dataFile.getInputStream(), pathFile);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
        FileDto fileDto = new FileDto(url, name);


        return fileDto;
    }

    @PostMapping("/accept1")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.PHE_DUYET + "')")
    public ResultDto accep1t(@RequestBody Poster poster) throws ParseException {
        Poster current = posterService.getById(poster.getId());
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Employee employee = employeeService.getById(Math.toIntExact(current.getEmployeeId()));


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date currentDate = new Date();

        String startDate = dateFormat.format(currentDate);
        String endDate = dateFormat.format(current.getDuration());

        Date date1 = dateFormat.parse(startDate);
        Date date2 = dateFormat.parse(endDate);

        long getDiff = date2.getTime() - date1.getTime();

        long getDaysDiff = getDiff / (24 * 60 * 60 * 1000);

        long money = getDaysDiff * 10000L;
        log.info("getDiff:{}",employee.getSurplus());

        if (employee.getSurplus() >= money) {

            employee.setSurplus(employee.getSurplus() - money);
            current.setStatus(2);
            posterService.save(current);
            actionHistoryService.save("Phê duyệt ", "Phê duyệt Bất động sản thành công", TABLE_NAME, current.getId(), account.getUsername());
            return new ResultDto(ResultDto.ResultCode.SUCCESS, "Phê duyệt thành công.");

        }

        Locale usa = new Locale("en", "VN");
        NumberFormat dollarFormat = NumberFormat.getCurrencyInstance(usa);

        current.setStatus(4);
        posterService.save(current);
        actionHistoryService.save("Phê duyệt ", "Số dư không đủ.", TABLE_NAME, current.getId(), account.getUsername());
        return new ResultDto(ResultDto.ResultCode.FAIL, "Số dư trong tài khoản không đủ " + dollarFormat.format(money));





    }


    @PostMapping("/accept")
    @PreAuthorize("hasAnyAuthority('" + Roles.System.Post.PHE_DUYET + "', '" + Roles.System.Post.THANH_VIEN_PHE_DUYET + "')")
    public ResultDto accept(@RequestBody Poster poster) {
        Poster current = posterService.getById(poster.getId());
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       if (poster.getStatus() == 0) {
            current.setStatus(0);
            current.setRefusal(poster.getRefusal());
            posterService.save(current);
            actionHistoryService.save("Ngừng hoạt động", "Ngừng bán Bất động sản", TABLE_NAME, current.getId(), account.getUsername());
        } else if (poster.getStatus() == 4) {
            current.setStatus(4);
            current.setRefusal(poster.getRefusal());
            posterService.save(current);
            actionHistoryService.save("Từ chối phê duyệt", "Từ chối phê duyệt Bất động sản", TABLE_NAME, current.getId(), account.getUsername());
        }
        return null;
    }


    /**
     * ------------------VISITOR----------------
     **/
    @GetMapping("/visitor/list")
    public PageDto<PosterDto> getPage(
            @RequestParam(value = "numBedRooms", required = false) Integer numBedRooms,
            @RequestParam(value = "numBathRooms", required = false) Integer numBathRooms,
            @RequestParam(value = "type", required = false) Integer type,
            @RequestParam(value = "typeProduct", required = false) Integer typeProduct,
            @RequestParam(value = "province", required = false) Integer province,
            @RequestParam(value = "district", required = false) Integer district,
            @RequestParam(value = "ward", required = false) Integer ward,
            @RequestParam(value = "level", required = false) Integer level,
            @RequestParam(value = "levelArea", required = false) Integer levelArea,
            @RequestParam(value = "direction", required = false) String direction,
            @RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        int pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return posterService.getPagePoster(numBedRooms, numBathRooms, type, typeProduct, province, district, ward, level, levelArea, direction, pageable);
    }

    @PostMapping("/visitor/addMessage")
    public void addMessage(@RequestBody PosterMessage posterMessage) {
        posterMessageService.save(posterMessage);
    }

    @GetMapping("/visitor/detail")
    public PosterDetailsDto getDetails(@RequestParam("posterId") Long posterId) {
        return posterService.getDetails(posterId);
    }

    @PostMapping("/visitor/view")
    public void view(@RequestParam("posterId") Long posterId) {
        Poster poster = posterService.getByIdStatus(posterId);
        Long view = poster.getTotalView() + 1;
        poster.setTotalView(view);
    }

    @GetMapping("/oderByTypeProduct")
    public List<PosterOderByTypeProductDto> oderByTypeProduct(@RequestParam(value = "type", required = false) Integer type,
                                                              @RequestParam(value = "typeProduct", required = false) Integer typeProduct) {
        return posterService.getOderByTypeProduct(type, typeProduct);
    }

    @GetMapping("/oderByAddress")
    public List<PosterOderByAddressDto> oderByAddress(@RequestParam(value = "type", required = false) Integer type,
                                                      @RequestParam(value = "typeProduct", required = false) Integer typeProduct) {
        return posterService.getOderByAddress(type, typeProduct);
    }

    @GetMapping(value = "/dataTypeProduct")
    public List<PosterOderByTypeProductDto> data() {
        return posterService.getDataSelect();
    }

    @GetMapping(value = "/oderByPrice")
    public List<PosterOderByPiceDto> dataPrice(@RequestParam(value = "type", required = false) Integer type,
                                               @RequestParam(value = "typeProduct", required = false) Integer typeProduct) {
        return posterService.getOderByPrice(type, typeProduct);
    }
}
