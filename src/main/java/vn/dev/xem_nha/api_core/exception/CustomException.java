package vn.dev.xem_nha.api_core.exception;

import vn.dev.xem_nha.api_core.common.ServiceError;
import lombok.Getter;
import vn.dev.xem_nha.api_core.constant.MessageCodes;

public class CustomException extends RuntimeException {
    @Getter
    private final ServiceError serviceError;

    public CustomException(ServiceError serviceError) {
        super(serviceError.getMessage());
        this.serviceError = serviceError;
    }

    public CustomException(String message) {
        super(message);
        this.serviceError = new ServiceError(MessageCodes.ERROR, message);
    }
}
