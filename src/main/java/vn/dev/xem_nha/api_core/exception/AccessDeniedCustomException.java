package vn.dev.xem_nha.api_core.exception;

import vn.dev.xem_nha.api_core.common.ServiceError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

public class AccessDeniedCustomException extends RuntimeException {
    @Getter
    private final ServiceError serviceError;

    public AccessDeniedCustomException(ServiceError serviceError) {
        super(serviceError.getMessage());
        this.serviceError = serviceError;
    }

    public AccessDeniedCustomException(String message) {
        super(message);
        this.serviceError = new ServiceError(HttpStatus.FORBIDDEN.value(), message);
    }
}
