package vn.dev.xem_nha.api_core.exception.handler;


import org.springframework.beans.factory.annotation.Autowired;
import vn.dev.xem_nha.api_core.common.ServiceAttributes;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import vn.dev.xem_nha.api_core.exception.AccessDeniedCustomException;
import vn.dev.xem_nha.api_core.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import vn.dev.xem_nha.api_core.service.MessageConfigService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@ControllerAdvice
@Slf4j
@Order(Ordered.LOWEST_PRECEDENCE)
public class GenericExceptionHandler {

    @Resource
    private HttpServletRequest httpServletRequest;

    @Autowired MessageConfigService messageConfigService;

    /**
     * @param exception
     * @return ServiceResponse
     * @apiNote handle all exceptions
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ServiceResponse> handleException(Exception exception) {
        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);
        log.error(String.format("requestId: %s, GenericException: %s", requestId, exception.getMessage()), exception);

        return ResponseEntity.ok(new ServiceResponse(requestId, new Date(),messageConfigService.getServiceErrorByCode(MessageCodes.ERROR)));
    }

    /**
     * @param ex
     * @return ServiceResponse
     * @apiNote handle invalid request exception
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ServiceResponse> handleValidationExceptions(HttpMessageNotReadableException ex) {
        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);
        log.error(String.format("requestId: %s, HttpMessageNotReadableException: %s", requestId, ex.getMessage()), ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ServiceResponse(requestId, new Date(), messageConfigService.getServiceErrorByCode(MessageCodes.BAD_REQUEST)));
    }

    /**
     * @param ex
     * @return ServiceResponse
     * @apiNote handle access denied exception (not permission)
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ServiceResponse> handleException(AccessDeniedException ex) {
        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ServiceResponse(requestId, new Date(), messageConfigService.getServiceErrorByCode(MessageCodes.FORBIDDEN)));
    }

    /**
     * @param ex
     * @return ServiceResponse
     * @apiNote handle custom exception
     */
    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ServiceResponse> handleValidationExceptions(CustomException ex) {
        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);
        log.warn(String.format("requestId: %s, CustomException: %s", requestId, ex.getMessage()));
//        return ResponseEntity.ok(new ServiceResponse(requestId, new Date(), ex.getServiceError()));
        return ResponseEntity.ok(new ServiceResponse(requestId, new Date(), MessageCodes.ERROR, ex.getMessage()));
    }

    @ExceptionHandler(AccessDeniedCustomException.class)
    public ResponseEntity<ServiceResponse> handleException(AccessDeniedCustomException ex) {
        String requestId = (String) httpServletRequest.getAttribute(ServiceAttributes.REQUEST_ID);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ServiceResponse(requestId, new Date(), ex.getServiceError()));
    }

}
