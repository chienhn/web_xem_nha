package vn.dev.xem_nha.api_core.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PageDto<T> {
    private int pageIndex;
    private int pageSize;
    private int totalPages;
    private long totalRecords;
    private long beginIndex;
    private long endIndex;
    private List<T> data;

    public static <T> PageDto<T> of(Page page, List<T> data) {
        PageDto<T> pageDto = new PageDto<>();
        pageDto.pageIndex = page.getPageable().getPageNumber() + 1;
        pageDto.pageSize = page.getPageable().getPageSize();
        pageDto.totalPages = page.getTotalPages();
        pageDto.totalRecords = page.getTotalElements();
        pageDto.beginIndex = page.getPageable().getOffset();
        pageDto.endIndex = page.getPageable().getOffset() + page.getNumberOfElements();
        pageDto.data = data;
        return pageDto;
    }
}
