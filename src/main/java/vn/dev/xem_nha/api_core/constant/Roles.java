package vn.dev.xem_nha.api_core.constant;

public class Roles {

    /**
     * TRANG CHỦ
     */
    public static class Home {
        public static final String PAGE_ID = "1";
        public static final String VIEW = "10";
    }

    /**
     * HỆ THỐNG
     */
    public static class System {
        /**
         * NGƯỜI DÙNG
         */
        public static class Account {
            public static final String VIEW_LIST = "160";
            public static final String XOA_NHOM_NGUOI_DUNG = "12";
            public static final String CAP_NHAT_NHOM_NGUOI_DUNG = "11";
            public static final String THEM_MOI_NHOM_NGUOI_DUNG = "10";
            public static final String XOA_NGUOI_DUNG = "7";
            public static final String DANH_SACH_NGUOI_DUNG = "3";
            public static final String CAP_NHAT_NGUOI_DUNG = "6";
            public static final String THEM_MOI_NGUOI_DUNG = "5";
            public static final String CHI_TIET_NHOM_NGUOI_DUNG = "9";
        }

        public static class Post {
            public static final String THEM_MOI_MEMBER = "42";
            public static final String CAP_NHAT_MEMBER = "43";
            public static final String LIST = "20";
            public static final String LIST_MEMBER = "41";
            public static final String TIN_NHAN = "16";
            public static final String TIN_NHAN_MEMBER = "44";
            public static final String CHI_TIET_TIN_NHAN = "18";
            public static final String CHI_TIET_TIN_NHAN_MEMBER = "45";

            public static final String PHE_DUYET = "25";
            public static final String THANH_VIEN_PHE_DUYET = "27";
        }

        public static class Recharge {
            public static final String THANH_VIEN_NAP_TIEN = "46";
            public static final String DANH_SACH = "21";
            public static final String DANH_SACH_MANAGER = "24";

            public static final String CHI_TIET = "22";
            public static final String ADMIN_CHI_TIET = "26";
            public static final String PHE_DUYET = "23";

        }
        public static class Bank {
            public static final String THEM_MOI = "15";
            public static final String DANH_SACH = "47";
            public static final String CAP_NHAT = "16";
            public static final String XOA = "18";

        }

        public static class Banner {
            public static final String THEM_MOI = "13";
            public static final String CHI_TIET = "14";

        }
    }
}
