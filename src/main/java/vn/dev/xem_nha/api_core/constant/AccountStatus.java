package vn.dev.xem_nha.api_core.constant;

public enum AccountStatus {
    INACTIVE, ACTIVE, BLOCKED
}
