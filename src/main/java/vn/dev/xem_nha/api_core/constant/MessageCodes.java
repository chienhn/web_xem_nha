package vn.dev.xem_nha.api_core.constant;

/**
 * @Describe: File config mã lỗi, map code với message trong bảng message_config trong DB
 * @Author: Hoa
 * */


public class MessageCodes {
    /**
     * COMMON ERRORS
     */
    public static final Integer SUCCESS = 0;
    public static final Integer ERROR = 99;
    public static final Integer BAD_REQUEST = 1;
    public static final Integer FORBIDDEN = 403;
    public static final Integer METHOD_NOT_ALLOWED = 405;

    /**
     * ACCOUNT ERRORS
     */
    public interface Account {
        Integer USERNAME_OR_PASSWORD_NOT_CORRECT = 1000;
        Integer ACCOUNT_INACTIVE = 1001;
        Integer ACCOUNT_NON_APPROVED = 1002;
        Integer ACCOUNT_NOT_FOUND = 1003;
        Integer PASSWORD_NOT_CORRECT = 1004;
        Integer ACCOUNT_LOCKED = 1005;
        Integer ACCOUNT_NOT_FOUND_BY_ID = 1006;
        Integer RESET_PASSWORD_SUCCESS = 1007;
        Integer ACCOUNT_EXISTED = 1009; // Tài khoản đã tồn tại
        Integer SAVE_NEW_SUCCESS = 1010; //Thêm mới tài khoản thành công
        Integer REGISTER_SUCCESS = 1016; //Đăng ký tài khoản thành công
        Integer UPDATE_GROUP_FOR_ACCOUNT_SUCCESS = 1011; // Cập nhật group cho account thành công
        Integer CHANGED_PASSWORD_SUCCESS = 1012; // Thay đổi mật khẩu thành công
        Integer DELETE_ACCOUNT_SUCCESS = 1013; // Xóa tài khoản thành công
        Integer PASSWORD_OLD_INVALID = 1014; // Mật khẩu cũ không được null hoặc trống
        Integer PASSWORD_OLD_WRONG = 1015; // Mật khẩu cũ không đúng
        Integer MASTER_ACCOUNT_CANNOT_DELETE = 1017; // Không có quyền xóa tài khoản chủ
    }

    /**
     * GROUP ACCOUNT ERRORS
     */
    public static final Integer GROUP_NOT_FOUND = 1020; // Nhóm tài khoản không tồn tại

    public interface Recharge {
        Integer TRANSACTION_CODE_EXISTED = 1800; // Mã giao dịch đã tồn tại
    }
}
