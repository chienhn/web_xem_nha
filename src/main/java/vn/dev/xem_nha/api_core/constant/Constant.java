package vn.dev.xem_nha.api_core.constant;

public class Constant {
    public static int MAX_LOGIN_FAIL = 6;
    public static class ActionHistory{
        public interface Action {
            String THEM_MOI = "Thêm mới";
        }
        public interface BannerHome {
            String THANH_CONG = "Thêm mới banner thành công";
            String TABLE_NAME = "BANNERHOME";
        }
    }
    public static class Status {
        public interface Account {
            int INACTIVE = 0;
            int ACTIVE = 1;
        }
    }
    public static class ResultMessage{
        public interface BannerHome {
            String THANH_CONG = "Thêm banner thành công";
        }
        public interface Bank {
            String THANH_CONG = "Thêm mới tài khoản thanh toán thành công";
            String CAP_NHAT = "Cập nhật tài khoản thanh toán thành công";
            String THAT_BAI = "Số tài khoản thanh toán đã tồn tại";
        }
    }
}
