package vn.dev.xem_nha.api_core.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "poster_message")
public class PosterMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "poster_id")
    private Long posterId;

    private String name;
    private String email;
    private String phone;
    private String message;
    private Integer status;

    @Column(name = "create_time", updatable = false)
    @CreationTimestamp
    private Date createTime;

    @PrePersist
    private void onCreate(){
        createTime = new Date();
        status = 0;
    }
}
