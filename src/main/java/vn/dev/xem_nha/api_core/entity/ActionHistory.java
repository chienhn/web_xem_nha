package vn.dev.xem_nha.api_core.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="action_history")
public class ActionHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "table_id")
    private Long tableId;

    @Column(name = "action")
    private String action;

    private String content;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "created_by")
    private String createdBy;     //id bảng account

    @Column(name = "created_at")
    private Date createdAt;

}
