package vn.dev.xem_nha.api_core.entity.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "privilege_group_page_role")
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupPageRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "group_id")
    private Integer groupId;

    @Column(name = "page_id")
    private Integer pageId;

    @Column(name = "role_id")
    private Integer roleId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        GroupPageRole that = (GroupPageRole) o;

        if (!Objects.equals(id, that.id)) return false;
        if (!Objects.equals(groupId, that.groupId)) return false;
        if (!Objects.equals(pageId, that.pageId)) return false;
        return Objects.equals(roleId, that.roleId);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(id);
        result = 31 * result + (Objects.hashCode(groupId));
        result = 31 * result + (Objects.hashCode(pageId));
        result = 31 * result + (Objects.hashCode(roleId));
        return result;
    }
}
