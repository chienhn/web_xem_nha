package vn.dev.xem_nha.api_core.entity.address;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "districts")
@Data
public class Districts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "slug")
    private String slug;

    @Column(name = "name_with_type")
    private String nameWithType;

    @Column(name = "path")
    private String path;

    @Column(name = "path_with_type")
    private String pathWithType;

    @Column(name = "code")
    private String code;

    @Column(name = "parent_code")
    private String parentCode;
}