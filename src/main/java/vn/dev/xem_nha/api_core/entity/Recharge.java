package vn.dev.xem_nha.api_core.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "recharge")
public class Recharge {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "content_recharge")
    private String contentRecharge;

    @Column(name = "money")
    private Long money;

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "created_at", updatable = false)
    private Date createdAt;

    @Column(name = "trading_code")
    private String tradingCode;

    @Column(name = "code")
    private String code;

    @Column(name = "bank_id")
    private Long bankId;


    @Column(name = "note")
    private String note;



}
