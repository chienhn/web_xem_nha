package vn.dev.xem_nha.api_core.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "poster")
public class Poster{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_id")
    private Long employeeId;

    private String title;
    private String description;
    private Float acreage;

    @Column(name = "total_bedroom")
    private Integer totalBedrooms;

    @Column(name = "total_bathroom")
    private Integer totalBathRooms;

    @Column(name = "total_floor")
    private Integer totalFloors;

    @Column(name = "total_view")
    private Long totalView;

    private Long price;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "zip_url")
    private String zipUrl;

    private Integer status;
    private Integer type;

    @Column(name = "type_product")
    private Integer typeProduct;

    @Column(name = "level")
    private Integer level;

    @Column(name = "year_building")
    private String yearBuilding;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "level_area")
    private Integer levelArea;

    private String refusal;
    private String direction;
    @Column(name = "posted_time")
    private Date postedTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private Integer province;
    private Integer district;
    private Integer ward;
    private Date duration;

    @PrePersist
    private void onCreate(){
        totalView = 0l;
        if (status == 2){
            postedTime = new Date();
        }
    }

    @PreUpdate
    private void onUpdate(){
        if (status == 2){
            postedTime = new Date();
        }
    }

}

