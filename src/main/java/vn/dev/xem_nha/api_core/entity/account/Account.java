package vn.dev.xem_nha.api_core.entity.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Hibernate;
import org.springframework.security.core.context.SecurityContextHolder;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.AccountStatus;
import vn.dev.xem_nha.api_core.entity.BaseEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "account")
@NoArgsConstructor
@AllArgsConstructor
public class Account extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "is_admin")
    private Integer isAdmin;

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private AccountStatus status;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;



    @OneToMany(fetch = FetchType.EAGER, mappedBy = "account", cascade = CascadeType.ALL)
    private List<AccountGroup> accountGroups;

    public Account(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        boolean result;
        if (this == o) {
            result = true;
        } else if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) {
            result = false;
        } else {
            Account account = (Account) o;
            result = Objects.equals(id, account.id);
        }

        return result;
    }

    public void setPassword(String password) {
        this.password = DigestUtils.md5Hex(password);
    }

    @Override
    public int hashCode() {
        return 2083479647;
    }

    @PrePersist
    protected void onCreate() {
        if (createTime == null) {
            createTime = new Date();
        }
        if (updateTime == null) {
            updateTime = new Date();
        }
        if (status == null) {
            status = AccountStatus.INACTIVE;
        }

        if (createBy == null) {
            try {
                AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                createBy = accountDetail.getAccount().getUserName();
            } catch (Exception e) {
            }
        }
    }

}
