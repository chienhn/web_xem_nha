package vn.dev.xem_nha.api_core.entity.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.dev.xem_nha.api_core.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "employee")
@AllArgsConstructor
@NoArgsConstructor
public class Employee extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "full_name")
    private String fullName;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "gender")
    private Integer sex;

    @Column(name = "province")
    private Integer province;

    @Column(name = "district")
    private Integer district;

    @Column(name = "ward")
    private Integer ward;

    @Column(name = "phone_number")
    private String workPhoneNumber;

    @Column(name = "address")
    private String address;

    private String email;

    @Column(name = "last_login")
    private Date lastLogin;

    @Column(name = "is_online")
    private Integer isOnline;

    @Column(name = "approval_status")
    private Integer approvalStatus;// trạng thái duyệt

    @Column(name = "surplus")
    private Long surplus;



}
