package vn.dev.xem_nha.api_core.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.context.SecurityContextHolder;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "banner_home")
public class BannerHome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "enable")
    private Integer enable;

    @Column(name = "create_time",updatable = false)
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "update_time")
    @UpdateTimestamp
    private Date updatedAt;

    @Column(name = "create_by")
    private String createdBy;

    @Column(name = "update_by")
    private String updatedBy;

    @Column(name = "order_no")
    private Integer stt;

    @Column(name = "date_apply")
    private Date dateApply;

    @PrePersist
    private void onCreate() {
        createdAt = new Date();
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        createdBy = accountDetail.getAccount().getUserName();
    }

    @PreUpdate
    private void onUpdate(){
        updatedAt = new Date();
    }
}
