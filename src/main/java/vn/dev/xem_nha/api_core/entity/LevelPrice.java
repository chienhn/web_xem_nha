package vn.dev.xem_nha.api_core.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "level_price")
public class LevelPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;
}
