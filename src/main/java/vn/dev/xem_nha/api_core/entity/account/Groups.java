package vn.dev.xem_nha.api_core.entity.account;

import vn.dev.xem_nha.api_core.entity.BaseEntity;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "privilege_group")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Groups extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private Integer status;

    @OneToMany(mappedBy = "group")
    @ToString.Exclude
    private List<AccountGroup> accountGroups;

    public Groups(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Groups groups = (Groups) o;

        return Objects.equals(id, groups.id);
    }

    @Override
    public int hashCode() {
        return 976642380;
    }
}
