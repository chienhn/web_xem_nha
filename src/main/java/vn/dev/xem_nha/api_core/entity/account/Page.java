package vn.dev.xem_nha.api_core.entity.account;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "page")
@Getter
@Setter
public class Page implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "page_name")
    private String pageName;

    @Column(name = "page_url")
    private String pageUrl;

    @Column(name = "page_icon")
    private String pageIcon;

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "level")
    private Integer level;



    @OneToMany(fetch = FetchType.EAGER, mappedBy = "page", cascade = CascadeType.ALL)
//    @OneToMany(mappedBy = "page")
    private List<Role> roles;

}
