package vn.dev.xem_nha.api_core.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity{
    @Column(name = "create_time", updatable = false)
    @CreatedDate
    protected Date createTime;

    @Column(name = "create_by", updatable = false)
    @CreatedBy
    protected String createBy;

    @Column(name = "update_time")
    @LastModifiedDate
    protected Date updateTime;

    @Column(name = "update_by")
    @LastModifiedBy
    protected String updateBy;
}
