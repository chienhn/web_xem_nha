package vn.dev.xem_nha.api_core.entity;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
@Table(name = "message_config")
public class MessageConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer code;

    private String message;
}
