package vn.dev.xem_nha.api_core.entity.account;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "page_menu")
@AllArgsConstructor
@NoArgsConstructor
public class PageMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "page_id")
    private Integer pageId;

    @Column(name = "menu_id")
    private Integer menuId;

    @Column(name = "description")
    private String description;

}
