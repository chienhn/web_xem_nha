package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.Constant;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeNewDto;
import vn.dev.xem_nha.api_core.dto.banner.SlideShowDto;
import vn.dev.xem_nha.api_core.dto.poster.PosterDetailsDto;
import vn.dev.xem_nha.api_core.entity.BannerHome;
import vn.dev.xem_nha.api_core.repository.BannerHomeRepo;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.BannerHomeService;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service(value = "bannerHomeService")
public class BannerHomeServiceImpl implements BannerHomeService {
    @Autowired BannerHomeRepo bannerHomeRepo;
    @Autowired
    ActionHistoryService actionHistoryService;

    final static String TABLE_NAME = Constant.ActionHistory.BannerHome.TABLE_NAME;


    DateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    DateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    @Override
    public PageDto<BannerHomeDto> getPage(Date fromDate, Date toDate, Integer enable, Pageable pageable) {
        List<Object[]> listBanner = bannerHomeRepo.getList(fromDate, toDate, enable);

        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listBanner.size());

        List<Object[]> pageObjects = listBanner.subList(start, end);
        List<BannerHomeDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:pageObjects){
            Long _id = eachObj[0] != null ? ((BigInteger) eachObj[0]) .longValue() : null;
            String _imageUrl = eachObj[1] != null ? (String) eachObj[1] : null;
            Integer _enabled = eachObj[2] != null ? (Integer) eachObj[2]: null;
            String _createdDate = null;
            try {
                if(eachObj[3] != null){
                    Date createdDate = (Date) eachObj[3] ;

                    _createdDate = sdf.format(createdDate);
                }
            } catch (Exception e) {}

            String _updatedDate = null;
            try {
                if(eachObj[4] != null){
                    Date updatedDate = (Date) eachObj[4] ;

                    _updatedDate = sdf.format(updatedDate);
                }
            } catch (Exception e) {}

            String _createdBy = eachObj[5] != null ? (String) eachObj[5] : null;
            String _updatedBy = eachObj[6] != null ? (String) eachObj[6] : null;
            Integer _stt = eachObj[7] != null ? (Integer) eachObj[7] : null;

            String _apllyDate = null;
            try {
                if(eachObj[8] != null){
                    Date applyDate = (Date) eachObj[8] ;

                    _apllyDate = sdf1.format(applyDate);
                }
            } catch (Exception e) {}

            BannerHomeDto discountDto=new BannerHomeDto(_id, _imageUrl, _enabled,_createdDate, _updatedDate, _createdBy, _updatedBy, _stt, _apllyDate);
            listResponse.add(discountDto);
        }

        PageImpl<BannerHomeDto> pageResponse=new PageImpl<>(listResponse,pageable,listBanner.size());
        PageDto<BannerHomeDto> pageDto = PageDto.of(pageResponse,pageResponse.getContent());
        return pageDto;
    }

    @Override
    public ResultDto createBanner(BannerHomeNewDto bannerHomeNewDto) {
        BannerHome bannerHome = convertFromBannerHomeNewDto(bannerHomeNewDto);
        save(bannerHome);
        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long idBannerHome = bannerHome.getId();
        actionHistoryService.save(Constant.ActionHistory.Action.THEM_MOI, Constant.ActionHistory.BannerHome.THANH_CONG, TABLE_NAME, idBannerHome, account.getUsername());
        return new ResultDto(ResultDto.ResultCode.SUCCESS, Constant.ResultMessage.BannerHome.THANH_CONG);
    }

    @Override
    public void save(BannerHome bannerHome) {

        bannerHomeRepo.save(bannerHome);
    }

    @Override
    public List<BannerHome> getListStart() {
        return bannerHomeRepo.getListBannerHomeStart();
    }

    @Override
    public List<BannerHome> getBannerHomeEnable(Integer stt) {
        return bannerHomeRepo.getBannerHomeEnable(stt);
    }

    @Override
    public BannerHomeDto findBanner(Long id) {
        List<Object[]> objects = bannerHomeRepo.detailBanner(id);
        Object[] eachObj = objects.get(0);
        Long _id = eachObj[0] != null ? ((BigInteger) eachObj[0]).longValue() : null;
        String _imageUrl = eachObj[1] != null ? (String) eachObj[1] : null;
        Integer _enabled = eachObj[2] != null ? (Integer) eachObj[2]: null;
        String _createdDate = null;
        try {
            if(eachObj[3] != null){
                Date createdDate = (Date) eachObj[3] ;

                _createdDate = sdf.format(createdDate);
            }
        } catch (Exception e) {}

        String _updatedDate = null;
        try {
            if(eachObj[4] != null){
                Date updatedDate = (Date) eachObj[4] ;

                _updatedDate = sdf.format(updatedDate);
            }
        } catch (Exception e) {}

        String _createdBy = eachObj[5] != null ? (String) eachObj[5] : null;
        String _updatedBy = eachObj[6] != null ? (String) eachObj[6] : null;
        Integer _stt = eachObj[7] != null ? (Integer) eachObj[7] : null;

        String _apllyDate = null;
        try {
            if(eachObj[8] != null){
                Date applyDate = (Date) eachObj[8] ;

                _apllyDate = sdf1.format(applyDate);
            }
        } catch (Exception e) {}
        return new BannerHomeDto(_id, _imageUrl, _enabled,_createdDate, _updatedDate, _createdBy, _updatedBy, _stt, _apllyDate);
    }

    @Override
    public BannerHome convertFromBannerHomeNewDto(BannerHomeNewDto bannerHomeNewDto) {
        BannerHome bannerHome = new BannerHome();
        bannerHome.setImageUrl(bannerHomeNewDto.getImageUrl());
        bannerHome.setEnable(bannerHomeNewDto.getEnable());
        bannerHome.setStt(bannerHomeNewDto.getOrderNo());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");
        Date dateApply = null;
        //Date toDate = null;
        try {
            dateApply = dateFormat.parse(bannerHomeNewDto.getDateApply());

        } catch (Exception e) {
        }
        bannerHome.setDateApply(dateApply);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String Date = simpleDateFormat.format(bannerHome.getDateApply());
        LocalDate myDate = new LocalDate(Date);
        if(myDate.equals(LocalDate.now())) {
            List<BannerHome> configPriceExist = bannerHomeRepo.getBannerHomeEnable(bannerHome.getStt());
            if (configPriceExist.size() > 0) {
                for (BannerHome cf : configPriceExist) {
                    cf.setEnable(0);
                    cf.setUpdatedAt(new Date());
                    bannerHomeRepo.save(cf);
                }
            }
            bannerHome.setEnable(bannerHomeNewDto.getEnable());
        }
        else if(myDate.isAfter(LocalDate.now())) {
            List<BannerHome> configPriceExist = bannerHomeRepo.getBannerHomeEnable(bannerHome.getStt());
            if (configPriceExist.size() > 0) {
                for (BannerHome cf : configPriceExist) {
                    cf.setEnable(2);
                    cf.setUpdatedAt(new Date());
                    bannerHomeRepo.save(cf);
                }
            }
            bannerHome.setEnable(1);
        } else {
            bannerHome.setEnable(0);
        }
        return bannerHome;
    }

    @Override
    public List<SlideShowDto> slideshow() {
        List<Object[]> listBanner = bannerHomeRepo.slideShow();

        List<SlideShowDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:listBanner){
            String _imageUrl = eachObj[0] != null ? (String) eachObj[0] : null;
            Integer _stt = eachObj[1] != null ? (Integer) eachObj[1]: null;

            SlideShowDto discountDto=new SlideShowDto(_imageUrl, _stt);
            listResponse.add(discountDto);
        }


        return listResponse;
    }

}
