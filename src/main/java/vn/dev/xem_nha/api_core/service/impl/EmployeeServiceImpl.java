package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.config.loaders.CommonServiceProperties;
import vn.dev.xem_nha.api_core.dto.account.UserChangePassDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDetailDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDto;
import vn.dev.xem_nha.api_core.dto.poster.PosterDetailsEmployeeDto;
import vn.dev.xem_nha.api_core.entity.account.Employee;
import vn.dev.xem_nha.api_core.repository.EmployeeRepo;
import vn.dev.xem_nha.api_core.service.EmployeeService;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final Client client;
    private final CommonServiceProperties commonServiceProperties;
    @Autowired EmployeeRepo employeeRepo;

    public EmployeeServiceImpl(Client client, CommonServiceProperties commonServiceProperties) {
        this.client = client;
        this.commonServiceProperties = commonServiceProperties;
    }

    @Override
    public EmployeeDetailDto getDetail(Integer id) {
            List<Object[]> objects = employeeRepo.detailEmployee(id);
            Object[] eachObj = objects.get(0);
            Integer _id = eachObj[0] != null ? (Integer) eachObj[0] : null;
            String _fullName = eachObj[1] != null ? (String) eachObj[1] : null;
            String _avatar = eachObj[2] != null ? (String) eachObj[2]: null;
            Integer _gender = eachObj[3] != null ? (Integer) eachObj[3]: null;
            String _birthday = null;
            try {
                if(eachObj[4] != null){
                    Date createdDate = (Date) eachObj[4] ;
                    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    _birthday = sdf.format(createdDate);
                }
            } catch (Exception e) {}

            String _address = eachObj[5] != null ? (String) eachObj[5] : null;
            String _email = eachObj[6] != null ? (String) eachObj[6] : null;
        String _phone = eachObj[7] != null ? (String) eachObj[7] : null;

            return new EmployeeDetailDto(_id, _fullName, _avatar,_gender, _birthday, _address, _email, _phone);
    }

    @Override
    public Employee find(Integer employeeId) {
        return employeeRepo.findFirstByEmployeeId(employeeId);
    }

    @Override
    public PageDto<EmployeeDto> getList(Date fromDate, Date toDate, Integer gender, String name, Pageable pageable) {

        List<Object[]> listBanner = employeeRepo.getList(fromDate, toDate, gender,name);

        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listBanner.size());

        List<Object[]> pageObjects = listBanner.subList(start, end);
        List<EmployeeDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:pageObjects){
            String _fullName = eachObj[0] != null ? (String) eachObj[0] : null;
            String _avatar = eachObj[1] != null ? (String) eachObj[1] : null;
            Integer _gender = eachObj[2] != null ? (Integer) eachObj[2]: null;
            String _birthday = null;
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            try {
                if(eachObj[3] != null){
                    Date birthday = (Date) eachObj[3] ;
                    _birthday = dateFormat.format(birthday);
                }
            } catch (Exception e) {}
            String _address = eachObj[4] != null ? (String) eachObj[4] : null;
            Integer _id = eachObj[5] != null ? (Integer) eachObj[5]: null;
            Long _surplus = eachObj[6] != null ? ((BigInteger) eachObj[6]).longValue(): 0L;


            EmployeeDto discountDto=new EmployeeDto(_fullName, _avatar, _gender,_birthday, _address,_id, _surplus);
            listResponse.add(discountDto);
        }

        PageImpl<EmployeeDto> pageResponse=new PageImpl<>(listResponse,pageable,listBanner.size());
        PageDto<EmployeeDto> pageDto = PageDto.of(pageResponse,pageResponse.getContent());
        return pageDto;
    }

    @Override
    public ServiceResponse changePassword(UserChangePassDto userChangePassDto) {
        Response response = client
                .target(commonServiceProperties.getBaseUri())
                .path(commonServiceProperties.getEndpoint().getEmployee().getChangePass())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(userChangePassDto, MediaType.APPLICATION_JSON_TYPE));;
        return response.readEntity(ServiceResponse.class);

    }

    @Override
    public PosterDetailsEmployeeDto getDetailsPDto(Long id) {
        Map<String, Object> employeeDetailsPDto = employeeRepo.getDetailPDto(id);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(employeeDetailsPDto, PosterDetailsEmployeeDto.class);
    }

    @Override
    public void save(Employee employee) {
        employeeRepo.save(employee);
    }

    @Override
    public Employee getById(Integer id) {
        return employeeRepo.getEmployeeById(id);
    }
}
