package vn.dev.xem_nha.api_core.service.mapper;

import vn.dev.xem_nha.api_core.dto.account.GroupDetailDto;
import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.AccountGroup;
import vn.dev.xem_nha.api_core.entity.account.Groups;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class GroupMapper {
    public Groups toGroup(GroupDetailDto groupDetailDto) {
        Groups group = new Groups();
        group.setId(groupDetailDto.getGroup().getId());
        group.setGroupName(groupDetailDto.getGroup().getGroupName());
        group.setDescription(groupDetailDto.getGroup().getDescription());
        group.setStatus(groupDetailDto.getGroup().getStatus());
        group.setCreateBy(groupDetailDto.getGroup().getCreateBy());
        group.setCreateTime(groupDetailDto.getGroup().getCreateTime());
        group.setUpdateBy(groupDetailDto.getGroup().getUpdateBy());
        group.setUpdateTime(groupDetailDto.getGroup().getUpdateTime());
        group.setAccountGroups(groupDetailDto.getUsers() == null ? new ArrayList<>() : groupDetailDto.getUsers().stream().map(user -> {
            AccountGroup accountGroup = new AccountGroup();
            Account account = new Account();
            account.setId(user.getId());
            accountGroup.setGroup(group);
            accountGroup.setAccount(account);
            return accountGroup;
        }).collect(Collectors.toList()));

        return group;
    }
}
