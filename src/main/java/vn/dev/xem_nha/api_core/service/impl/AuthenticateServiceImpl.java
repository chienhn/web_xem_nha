package vn.dev.xem_nha.api_core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.config.loaders.CommonServiceProperties;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import vn.dev.xem_nha.api_core.dto.account.AccountDto;
import vn.dev.xem_nha.api_core.dto.account.GetAccountInfoResponse;
import vn.dev.xem_nha.api_core.entity.account.Account;
import vn.dev.xem_nha.api_core.entity.account.Page;
import vn.dev.xem_nha.api_core.entity.account.Role;
import vn.dev.xem_nha.api_core.exception.AccessDeniedCustomException;
import vn.dev.xem_nha.api_core.exception.CustomException;
import vn.dev.xem_nha.api_core.repository.AccountRepo;
import vn.dev.xem_nha.api_core.repository.PageRepo;
import vn.dev.xem_nha.api_core.repository.RoleRepo;
import vn.dev.xem_nha.api_core.service.AuthenticateService;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.service.MessageConfigService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AuthenticateServiceImpl implements AuthenticateService {
     private final Client client;
    private final HttpServletRequest httpServletRequest;
    private final AccountRepo accountRepo;
    private final PageRepo pageRepo;
    private final RoleRepo roleRepo;

    @Autowired MessageConfigService messageConfigService;

    public AuthenticateServiceImpl(Client client, HttpServletRequest httpServletRequest, AccountRepo accountRepo, PageRepo pageRepo, RoleRepo roleRepo) {
        this.client = client;
        this.httpServletRequest = httpServletRequest;
        this.accountRepo = accountRepo;
        this.pageRepo = pageRepo;
        this.roleRepo = roleRepo;
    }

    @Override
    public AccountDetail findByAccountId(Integer accountId) {
        if (!accountId.equals(-1)) {
            Account account = accountRepo.findById(accountId).orElseThrow(()
                    -> new CustomException(String.format(messageConfigService.getMessageByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND_BY_ID), accountId)));

            List<Role> roles = new ArrayList<>();
            account.getAccountGroups().forEach(accountGroup -> {
                Integer groupId = accountGroup.getGroup().getId();
                List<Page> pages = pageRepo.findAllByGroupId(groupId);
                pages.forEach(page -> {
                    List<Role> listRoleActiveByPage = roleRepo.findAllByGroupIdAndPageId(groupId, page.getId());
                    roles.addAll(listRoleActiveByPage);
                });
            });

            AccountDetail auth = new AccountDetail();
            auth.setAccount(account);
            auth.setRoles(roles.stream().map(role -> String.valueOf(role.getId())).distinct().collect(Collectors.toList()));
            return auth;
        }

        List<Role> roles = new ArrayList<>();
        AccountDetail auth = new AccountDetail();
        Account account = new Account();
        account.setId(-1);
        account.setUserName("anonymous");

        List<Page> pages = pageRepo.findAllByGroupId(-2);
        pages.forEach(page -> roles.addAll(page.getRoles()));

        auth.setAccount(account);
        auth.setRoles(roles.stream().map(role -> String.valueOf(role.getId())).distinct().collect(Collectors.toList()));

        return auth;
    }
}
