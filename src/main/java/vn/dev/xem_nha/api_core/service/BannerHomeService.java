package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeNewDto;
import vn.dev.xem_nha.api_core.dto.banner.SlideShowDto;
import vn.dev.xem_nha.api_core.entity.BannerHome;

import java.util.Date;
import java.util.List;

public interface BannerHomeService {
    PageDto<BannerHomeDto> getPage(Date fromDate, Date toDate, Integer enable, Pageable pageable);

    ResultDto createBanner(BannerHomeNewDto bannerHomeNewDto);

    BannerHome convertFromBannerHomeNewDto(BannerHomeNewDto bannerHomeNewDto);

    List<SlideShowDto> slideshow();

    void save(BannerHome bannerHome);

    List<BannerHome> getListStart();

    List<BannerHome> getBannerHomeEnable(Integer stt);

    BannerHomeDto findBanner(Long id);
}
