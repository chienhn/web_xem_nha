package vn.dev.xem_nha.api_core.service;

import vn.dev.xem_nha.api_core.entity.ActionHistory;

import java.util.List;

public interface ActionHistoryService {

    void save(String action, String content, String tableName, Long tableId, String createdBy);

    List<ActionHistory> listAllAction(String tableName, Long tableId);
}
