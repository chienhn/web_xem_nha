package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.ImageDto;
import vn.dev.xem_nha.api_core.dto.recharge.RechargeDto;
import vn.dev.xem_nha.api_core.entity.Recharge;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

public interface RechargeService {

    void add(Map<String, Object> mapParams);

    PageDto<RechargeDto> getPage(
            Date fromDate, Date toDate, Integer status, Long employeeId,
            Pageable pageable
    );
    PageDto<RechargeDto> getPageMana(
            Date fromDate, Date toDate, Integer status,
            Pageable pageable
    );

    Recharge getDetail(Long id);
    Long getSurplus(Long employeeId);

    void approve(Recharge recharge) throws ParseException;


    void save(Recharge recharge);

    int countOrderByMonth();

    boolean checkTradingCode(String tradingCode);
}
