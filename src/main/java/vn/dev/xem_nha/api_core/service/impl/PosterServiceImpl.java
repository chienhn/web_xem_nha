package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.poster.*;
import vn.dev.xem_nha.api_core.entity.Poster;
import vn.dev.xem_nha.api_core.repository.PosterRepo;
import vn.dev.xem_nha.api_core.service.EmployeeService;
import vn.dev.xem_nha.api_core.service.PosterService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service(value = "posterService")
public class PosterServiceImpl implements PosterService {

    @Autowired PosterRepo posterRepo;
    @Autowired EmployeeService employeeService;


    @Override
    public void save(Poster poster) {
        posterRepo.save(poster);
    }

    @Override
    public Poster getById(Long id) {
        return posterRepo.getPosterById(id);
    }
    @Override
    public Poster getByIdStatus(Long id) {
        return posterRepo.getByIdStatus(id);
    }

    @Override
    public PosterDetailsDto getDetails(Long id) {

        ObjectMapper objectMapper = new ObjectMapper();
        PosterDetailsDto posterDetailsDto = objectMapper.convertValue(posterRepo.getDetails(id), PosterDetailsDto.class);

        posterDetailsDto.setEmployeePDto(employeeService.getDetailsPDto(posterRepo.getPosterById(id).getEmployeeId()));
        return posterDetailsDto;
    }


    @Override
    public PageDto<PosterDto> getPagePoster(Integer numBedRooms, Integer numRestRooms, Integer type, Integer typeProduct, Integer province, Integer district, Integer ward, Integer level, Integer levelArea, String direction, Pageable pageable) {

        List<Map<String, Object>> listPoster = posterRepo.getListPoster(numBedRooms, numRestRooms, type, typeProduct, province, district, ward, level, levelArea, direction);

        List<PosterDto> listPosterDto = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : listPoster) {
            PosterDto posterDto = objectMapper.convertValue(map, PosterDto.class);
            posterDto.setImageUrl(posterDto.getImageUrl().split(",")[0].trim());
            listPosterDto.add(posterDto);
        }
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listPoster.size());
        List<PosterDto> listResponse = listPosterDto.subList(start, end);

        Page<PosterDto> pageResponse = new PageImpl<>(listResponse, pageable, listPosterDto.size());

        PageDto<PosterDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());

        return pageDto;
    }

    @Override
    public PageDto<PosterManagerDto> getPagePosterMana(Date fromDate, Date toDate, Integer status, Integer type, Integer typeProduct, Integer province, Pageable pageable) {

        List<Map<String, Object>> listPoster = posterRepo.getListPosterMana(fromDate, toDate, type, typeProduct, status, province);

        List<PosterManagerDto> listPosterDto = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : listPoster) {
            PosterManagerDto posterDto = objectMapper.convertValue(map, PosterManagerDto.class);
            posterDto.setEmployeePDto(employeeService.getDetailsPDto(posterRepo.getPosterById(posterDto.getId()).getEmployeeId()));
            listPosterDto.add(posterDto);
        }
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listPoster.size());
        List<PosterManagerDto> listResponse = listPosterDto.subList(start, end);

        Page<PosterManagerDto> pageResponse = new PageImpl<>(listResponse, pageable, listPosterDto.size());

        PageDto<PosterManagerDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());

        return pageDto;
    }

    @Override
    public PageDto<PosterManagerDto> getPagePosterManaMember(Date fromDate, Date toDate, Integer status, Integer type, Integer typeProduct, Integer province, Long employeeId, Pageable pageable) {

        List<Map<String, Object>> listPoster = posterRepo.getListPosterManaMember(fromDate, toDate, type, typeProduct, status, province, employeeId);

        List<PosterManagerDto> listPosterDto = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : listPoster) {
            PosterManagerDto posterDto = objectMapper.convertValue(map, PosterManagerDto.class);
            posterDto.setEmployeePDto(employeeService.getDetailsPDto(posterRepo.getPosterById(posterDto.getId()).getEmployeeId()));
            listPosterDto.add(posterDto);
        }
        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listPoster.size());
        List<PosterManagerDto> listResponse = listPosterDto.subList(start, end);

        Page<PosterManagerDto> pageResponse = new PageImpl<>(listResponse, pageable, listPosterDto.size());

        PageDto<PosterManagerDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());

        return pageDto;
    }
    @Override
    public List<PosterOderByTypeProductDto> getOderByTypeProduct(Integer type, Integer typeProduct) {
        List<Object[]> listBanner = posterRepo.getOderByTypeProduct(type, typeProduct);

        List<PosterOderByTypeProductDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:listBanner){
            String _name = eachObj[0] != null ? (String) eachObj[0] : null;
            Long _total = eachObj[1] != null ? (Long) eachObj[1]: null;
            Long _id = eachObj[2] != null ? (Long) eachObj[2]: null;
            PosterOderByTypeProductDto discountDto=new PosterOderByTypeProductDto(_name, _total, _id);
            listResponse.add(discountDto);
        }


        return listResponse;
    }

    @Override
    public List<PosterOderByTypeProductDto> getDataSelect() {
        List<Object[]> listBanner = posterRepo.getDataSelect();

        List<PosterOderByTypeProductDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:listBanner){
            String _name = eachObj[0] != null ? (String) eachObj[0] : null;
            Long _id = eachObj[1] != null ? (Long) eachObj[1]: null;
            PosterOderByTypeProductDto discountDto=new PosterOderByTypeProductDto(_name, null, _id);
            listResponse.add(discountDto);
        }


        return listResponse;
    }

    @Override
    public List<PosterOderByAddressDto> getOderByAddress(Integer type, Integer typeProduct) {
        List<Object[]> listBanner = posterRepo.getOderByAddress(type, typeProduct);

        List<PosterOderByAddressDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:listBanner){
            String _provinces = eachObj[0] != null ? (String) eachObj[0] : null;
            Long _total = eachObj[1] != null ? (Long) eachObj[1] : null;
            Long _id = eachObj[2] != null ? (Long) eachObj[2] : null;
            String _code = eachObj[3] != null ? (String) eachObj[3] : null;
            PosterOderByAddressDto discountDto=new PosterOderByAddressDto(_provinces, _total, _id,_code);
            listResponse.add(discountDto);
        }


        return listResponse;
    }

    @Override
    public List<PosterOderByPiceDto> getOderByPrice(Integer type, Integer typeProduct) {
        List<Object[]> listBanner = posterRepo.getOderByPrice(type, typeProduct);

        List<PosterOderByPiceDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:listBanner){
            String _provinces = eachObj[0] != null ? (String) eachObj[0] : null;
            Long _total = eachObj[1] != null ? (Long) eachObj[1] : null;
            Long _id = eachObj[2] != null ? (Long) eachObj[2] : null;
            PosterOderByPiceDto discountDto=new PosterOderByPiceDto(_provinces, _total, _id);
            listResponse.add(discountDto);
        }


        return listResponse;
    }

    @Override
    public List<Long> getAllIdPoster(Long empId) {
        return null;
    }

    @Override
    public List<Poster> getListStart() {
        return posterRepo.getListStart();
    }


}
