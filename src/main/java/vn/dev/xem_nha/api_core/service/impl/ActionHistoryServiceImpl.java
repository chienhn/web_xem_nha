package vn.dev.xem_nha.api_core.service.impl;


import vn.dev.xem_nha.api_core.entity.ActionHistory;
import vn.dev.xem_nha.api_core.repository.ActionHistoryRepo;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ActionHistoryServiceImpl implements ActionHistoryService {

    @Resource
    ActionHistoryRepo actionHistoryRepo;

    @Override
    public void save(String action, String content, String tableName, Long tableId, String createdBy) {
        log.info("tableName: {}", tableName);
        ActionHistory actionHis = new ActionHistory();
        actionHis.setTableId(tableId);
        actionHis.setAction(action);
        actionHis.setContent(content);
        actionHis.setTableName(tableName);
        actionHis.setCreatedBy(createdBy);
        actionHis.setCreatedAt(new Date());
        log.info("actionHis: {}", actionHis);
        actionHistoryRepo.save(actionHis);
    }



    @Override
    public List<ActionHistory> listAllAction(String tableName, Long tableId) {
        return actionHistoryRepo.findAllAction(tableName, tableId);
    }
}
