package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.AccountStatus;
import vn.dev.xem_nha.api_core.constant.Constant;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.account.*;
import vn.dev.xem_nha.api_core.entity.account.*;
import vn.dev.xem_nha.api_core.exception.CustomException;
import vn.dev.xem_nha.api_core.repository.*;
import vn.dev.xem_nha.api_core.service.AccountService;
import vn.dev.xem_nha.api_core.service.AuthenticateService;
import vn.dev.xem_nha.api_core.service.MessageConfigService;
import vn.dev.xem_nha.api_core.service.mapper.GroupMapper;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final JwtTokenProvider jwtService;
    private final PageRepo pageRepo;
    private final RoleRepo roleRepo;

    private final GroupRepo groupRepo;
    private final GroupMapper groupMapper;
    private final AccountGroupRepo accountGroupRepo;
    private final GroupPageRoleRepo groupPageRoleRepo;
    private final ModelMapper modelMapper;
    private final AuthenticateService authenticateService;
    @Autowired
    MessageConfigService messageConfigService;
    //    @Resource CustomerService customerService;
    @Value(value = "${account.default-passwd}")
    private String DEFAULT_PASSWORD;

    public AccountServiceImpl(AccountRepo accountRepo, JwtTokenProvider jwtService, PageRepo pageRepo, RoleRepo roleRepo, GroupRepo groupRepo, GroupMapper groupMapper, AccountGroupRepo accountGroupRepo, GroupPageRoleRepo groupPageRoleRepo, ModelMapper modelMapper, AuthenticateService authenticateService) {
        this.accountRepo = accountRepo;
        this.jwtService = jwtService;
        this.pageRepo = pageRepo;
        this.roleRepo = roleRepo;

        this.groupRepo = groupRepo;
        this.groupMapper = groupMapper;
        this.accountGroupRepo = accountGroupRepo;
        this.groupPageRoleRepo = groupPageRoleRepo;
        this.modelMapper = modelMapper;
        this.authenticateService = authenticateService;
    }

    @Override
    //@Transactional
    public AccountLoginResponse login(AccountLoginDto accountLoginDto) {
        Account account = accountRepo.getAccountByUsername(accountLoginDto.getUserName());
        if (account == null) {
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.USERNAME_OR_PASSWORD_NOT_CORRECT));
        }


        else if (account.getStatus() != AccountStatus.ACTIVE) {

            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.USERNAME_OR_PASSWORD_NOT_CORRECT));
        }

        //check password
        else if (!Objects.equals(account.getPassword(), DigestUtils.md5Hex(accountLoginDto.getPassword()))){

            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.PASSWORD_NOT_CORRECT));
        } else {

            if (account.getEmployee() != null) {
                account.getEmployee().setIsOnline(1);
            }
            accountRepo.save(account);
        }
        List<Page> pages = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        account
                .getAccountGroups()
                .forEach(accountGroup -> {
                    List<Page> pageList = pageRepo.findAllByGroupId(accountGroup.getGroup().getId());
                    pages.addAll(pageList);
                    pageList.forEach(p -> {
                        roles.addAll(roleRepo.findAllByGroupIdAndPageId(accountGroup.getGroup().getId(), p.getId()));
                    });
                });

        AccountLoginResponse accountLoginResponse = new AccountLoginResponse(jwtService, account, pages, roles);
//        Customer customer = customerService.findCustomerByAccountId(accountLoginResponse.getUser().getId().longValue());
//        if (customer != null) {
//            accountLoginResponse.getUser().setCustomerId(customer.getId());
//        }
        return accountLoginResponse;
    }

    @Override
    @Transactional
    public GroupPageRoleDto getPageRoles(Account auth, Integer menuId) {
        Account account =
                accountRepo
                        .findById(auth.getId())
                        .orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND)));
        if (account.getStatus() != AccountStatus.ACTIVE)
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_INACTIVE));

        List<Page> pages = new ArrayList<>();
        List<Role> roles = new ArrayList<>();


//        List<Groups> groups = groupRepo.getAllGroupByAccountId(account.getId());
//        log.info("amount group: {}, accountId: {}", groups.size(), account.getId());
//        for (Groups group : groups) {
//            List<Page> pageList = pageRepo.findAllByGroupId(group.getId());
//            log.info("group: {}, ", group.getGroupName());
//            pages.addAll(pageList);
//        }

        account.getAccountGroups().forEach(accountGroup -> {

            List<Page> pageList = pageRepo.findAllByGroupId(accountGroup.getGroup().getId());
            log.info("group: {}, ", accountGroup.getGroup().getGroupName());

            pages.addAll(pageList);

            pageList.forEach(pages1 -> {
                List<Role> roleList = roleRepo.findAllByGroupIdAndPageId(accountGroup.getGroup().getId(), pages1.getId());
                roles.addAll(roleList);
            });
        });

        GroupPageRoleDto groupPageRoleDto = new GroupPageRoleDto(account, pages, roles);

        groupPageRoleDto
                .setPageRoles(groupPageRoleDto.getPageRoles()
                        .stream()
                        .sorted(Comparator.comparingInt(PageRoleDto::getLevel))
                        .collect(Collectors.toList()));

        return groupPageRoleDto;
    }

    @Override
    public PageDto<AccountLstDto> getList(Integer userId, Integer pageIndex, Integer pageSize, Integer status) {
        Integer pageNumber = 0;
        if (pageIndex > 0) {
            pageNumber = pageIndex - 1;
        }
        Pageable _pageable = PageRequest.of(pageNumber, pageSize);


        List<Map<String, Object>> allAccount = accountRepo.getPage(status);

        final int start = (int) _pageable.getOffset();
        final int end = Math.min((start + _pageable.getPageSize()), allAccount.size());

        List<Map<String, Object>> pageObjects = allAccount.subList(start, end);

        List<AccountLstDto> resultList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : pageObjects) {
            AccountLstDto accountDto = objectMapper.convertValue(map, AccountLstDto.class);
            resultList.add(accountDto);
        }

        PageImpl<AccountLstDto> pageResponse = new PageImpl<>(resultList, _pageable, allAccount.size());
        PageDto<AccountLstDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());
        return pageDto;
    }


    @Override
    @Transactional
    public AccountDetailDto getDetail(Integer userId) {
        Account account =
                accountRepo
                        .findById(userId)
                        .orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND)));

        List<Page> pages = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        account.getAccountGroups().forEach(accountGroup -> {
            List<Page> pageList = pageRepo.findAllByGroupId(accountGroup.getGroup().getId());
            pages.addAll(pageList);
            pageList.forEach(p -> {
                roles.addAll(roleRepo.findAllByGroupIdAndPageId(accountGroup.getGroup().getId(), p.getId()));
            });
        });

        return new AccountDetailDto(account, pages, roles);
    }

    @Override
    public ListPageRoleDto getListPages() {
        List<Page> pages = pageRepo.findAll();
        List<Role> roles = roleRepo.findAll();
        return new ListPageRoleDto(pages, roles);
    }

    @Override
    public List<GroupDto> getListGroups() {
        return groupRepo.findAll().stream().map(GroupDto::new).collect(Collectors.toList());
    }

    @Override
    public GroupDetailDto getDetailGroup(Integer groupId) {
        List<Role> roles = new ArrayList<>();
        List<Page> pages = pageRepo.findAllByGroupId(groupId);
        pages.forEach(p -> roles.addAll(roleRepo.findAllByGroupIdAndPageId(groupId, p.getId())));
        return new GroupDetailDto(
                groupRepo
                        .findById(groupId)
                        .orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.GROUP_NOT_FOUND))),
                pages,
                roles
        );
    }

    @Override
    public Map<String, Object> updateGroupForAccount(Integer accountId, String groupIdList, Integer status) {
        Map<String, Object> result = new HashMap<>();

        List<Integer> listGroupIdForSave = new ArrayList<>();
        if (groupIdList != null && !groupIdList.equals("")) {
            for (String strGroupId : groupIdList.split(",")) {
                listGroupIdForSave.add(Integer.valueOf(strGroupId));
            }
        }

        Account account = findFirstById(accountId);
        if (status == Constant.Status.Account.INACTIVE) {
            account.setStatus(AccountStatus.INACTIVE);

        } else if (status == Constant.Status.Account.ACTIVE) {
            account.setStatus(AccountStatus.ACTIVE);
        }

        List<AccountGroup> accountGroups = account.getAccountGroups();
        for (AccountGroup accountGroup : accountGroups) {
            boolean existsInListSave = false;
            for (Integer eachGroupId : listGroupIdForSave) {
                if (accountGroup.getGroup().getId().equals(eachGroupId)) {
                    existsInListSave = true;
                    break;
                }
            }
            if (!existsInListSave) {
                accountGroupRepo.deleteAcc(accountGroup.getId());
            }
        }

        if (listGroupIdForSave.size() > 0) {
            for (Integer eachGroupId : listGroupIdForSave) {
                boolean exists = false;
                for (AccountGroup accountGroup : accountGroups) {
                    if (accountGroup.getGroup().getId().equals(eachGroupId)) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    AccountGroup accountGroup = new AccountGroup();
                    accountGroup.setAccount(account);
                    accountGroup.setGroup(groupRepo.findById(eachGroupId).get());
                    accountGroupRepo.save(accountGroup);
                }
            }
        }


        result.put("message", messageConfigService.getMessageByCode(MessageCodes.Account.UPDATE_GROUP_FOR_ACCOUNT_SUCCESS));
        updateAccount(account);
        return result;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addGroup(GroupDetailDto groupDetailDto) {
        Groups groups = groupMapper.toGroup(groupDetailDto);
        groupRepo.save(groups);

        List<GroupPageRole> groupPageRoles = new ArrayList<>();
        groupDetailDto.getPageRoles().forEach(pageRoleDto -> {
            Arrays.asList(pageRoleDto.getRoles().split(",")).forEach(roleId -> {
                GroupPageRole groupPageRole = new GroupPageRole();
                groupPageRole.setGroupId(groupDetailDto.getGroup().getId());
                groupPageRole.setPageId(pageRoleDto.getId());
                groupPageRole.setRoleId(Integer.valueOf(roleId));
                groupPageRoles.add(groupPageRole);
            });
        });
        groupPageRoleRepo.saveAll(groupPageRoles);

        if (groups.getAccountGroups() != null) {
            accountGroupRepo.saveAll(groups.getAccountGroups());
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateGroup(GroupDetailDto groupDetailDto) {
        if (!groupRepo.existsById(groupDetailDto.getGroup().getId()))
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.GROUP_NOT_FOUND));
        Groups groups = groupRepo.findById(groupDetailDto.getGroup().getId())
                .orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.GROUP_NOT_FOUND)));

        accountGroupRepo.deleteAll(groups.getAccountGroups());
        groupPageRoleRepo.deleteAllByGroupId(groupDetailDto.getGroup().getId());

        groups = groupMapper.toGroup(groupDetailDto);

        boolean groupExists = groupRepo.existsByGroupName(groups.getGroupName());
        if (!groupExists) {
            groupRepo.save(groups);
        }

        List<GroupPageRole> groupPageRoles = new ArrayList<>();
        groupDetailDto.getPageRoles().forEach(pageRoleDto -> {
            // todo: lỗi trường hợp không có dấu , để bỏ tất cả role của group
            Arrays.asList(pageRoleDto.getRoles().split(",")).forEach(roleId -> {
                GroupPageRole groupPageRole = new GroupPageRole();
                groupPageRole.setGroupId(groupDetailDto.getGroup().getId());
                groupPageRole.setPageId(pageRoleDto.getId());
                groupPageRole.setRoleId(Integer.valueOf(roleId));

                groupPageRoles.add(groupPageRole);
            });
        });
        groupPageRoleRepo.saveAll(groupPageRoles);

//        if (groups.getAccountGroups() != null) {
//            accountGroupRepo.saveAll(groups.getAccountGroups());
//        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteGroup(Integer groupId) {
        Groups groups = groupRepo.findById(groupId).orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.GROUP_NOT_FOUND)));
        accountGroupRepo.deleteAll(groups.getAccountGroups());
        groupPageRoleRepo.deleteAllByGroupId(groupId);
        groupRepo.delete(groups);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addUserToGroup(GroupDetailDto groupDetailDto) {
        if (!groupRepo.existsById(groupDetailDto.getGroup().getId()))
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.GROUP_NOT_FOUND));
        groupRepo.save(groupMapper.toGroup(groupDetailDto));

    }

    @Override
    @Transactional
    public AccountDetail findById(Integer accountId) {
        Account account =
                accountRepo
                        .findById(accountId)
                        .orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND)));

        if (account.getStatus() != AccountStatus.ACTIVE)
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_INACTIVE));

//        List<String> roles = new ArrayList<>();
//        account
//                .getAccountGroups()
//                .forEach(accountGroup -> {
//                    List<Page> pageList = pageRepo.findAllByGroupId(accountGroup.getGroup().getId());
//                    pageList.forEach(page -> {
//                        List<Role> roleList = roleRepo.findAllByGroupIdAndPageId(accountGroup.getGroup().getId(), page.getId());
//                        roles.addAll(roleList.stream().map(role -> "ROLE_" + role.getId()).collect(Collectors.toList()));
//                    });
//                });
        List<String> roles = roleRepo
                .findAllByGroupsIn(account.getAccountGroups().stream().map(AccountGroup::getGroup).collect(Collectors.toList()))
                .stream()
                .distinct()
                .sorted(Comparator.comparingInt(Role::getId))
                .map(role -> "ROLE_" + role.getId())
                .collect(Collectors.toList());

        return new AccountDetail(account, roles);
    }

    @Override
    @Transactional
    public AccountDto getInfo(String accessToken, String clientId, String clientSecret) {
        Integer accountId = jwtService.getCustomerId(accessToken);
        Account account = accountRepo.findById(accountId).orElseThrow(() -> new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND)));
        return new AccountDto(account);
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void changePassword(UserChangePassDto userChangePassDto) {
        if (!accountRepo.existsByIdAndPassword(userChangePassDto.getUserId(), DigestUtils.md5Hex(userChangePassDto.getOldPassword())))
            throw new CustomException("Mật khẩu không chính xác !");

        Account account = accountRepo.findById(userChangePassDto.getUserId()).get();
        account.setPassword(userChangePassDto.getNewPassword());
        accountRepo.save(account);
    }

    @Override
    public Map<String, Object> addByManager(AccountCreateDto accountCreateDto) {
        Map<String, Object> result = new HashMap<>();

        Account account = findFirstByUserName(accountCreateDto.getUserName());
        if (account != null) {
            result.put("resultCode", ResultDto.ResultCode.FAIL);
            result.put("message", messageConfigService.getMessageByCode(MessageCodes.Account.ACCOUNT_EXISTED));
            return result;
        }

        List<Integer> listGroupIdForSave = new ArrayList<>();

        if (accountCreateDto.getGroupIdList() != null && !accountCreateDto.getGroupIdList().equals("")) {
            for (String strGroupId : accountCreateDto.getGroupIdList().split(",")) {
                listGroupIdForSave.add(Integer.valueOf(strGroupId));
            }
        }


        account = new Account();
        account.setUserName(accountCreateDto.getUserName());
        account.setPassword(accountCreateDto.getPassword());
        account.setIsAdmin(accountCreateDto.getIsAdmin());
        account.setCreateTime(new Date());
        account.setStatus(AccountStatus.ACTIVE);
        createOrUpdate(account);


        if (listGroupIdForSave.size() > 0) {
            for (Integer eachGroupId : listGroupIdForSave) {
                AccountGroup accountGroup = new AccountGroup();
                accountGroup.setAccount(account);
                accountGroup.setGroup(groupRepo.findById(eachGroupId).get());
                accountGroupRepo.save(accountGroup);
            }
        }


        result.put("resultCode", ResultDto.ResultCode.SUCCESS);
        result.put("message", messageConfigService.getMessageByCode(MessageCodes.Account.SAVE_NEW_SUCCESS));

        return result;
    }

    @Override
    public ResultDto register(AccountRegisterDto accountCreateDto) {
        ResultDto result = new ResultDto();

        Account account = findFirstByUserNameAndStatus(accountCreateDto.getUserName(), AccountStatus.BLOCKED);
        if (account != null) {
            result.setResultCode(ResultDto.ResultCode.FAIL);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.ACCOUNT_EXISTED));
            return result;
        }
        account = new Account();
        account.setUserName(accountCreateDto.getUserName());
        account.setPassword(accountCreateDto.getPassword());
        account.setCreateTime(new Date());
        account.setIsAdmin(0);
        account.setStatus(AccountStatus.ACTIVE);
        createOrUpdate(account);
        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(groupRepo.findById(7).get());
        accountGroupRepo.save(accountGroup);
        result.setResultCode(ResultDto.ResultCode.SUCCESS);
        result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.REGISTER_SUCCESS));

        return result;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ResultDto resetPassword(Integer userId) {
        String defaultPassword = DEFAULT_PASSWORD;
        Account account = accountRepo.findById(userId).orElseThrow(()
                -> new CustomException(String.format(messageConfigService.getMessageByCode(MessageCodes.Account.ACCOUNT_NOT_FOUND_BY_ID), userId)));
        account.setPassword(defaultPassword);
        accountRepo.save(account);

        return new ResultDto(ResultDto.ResultCode.SUCCESS,
                String.format(messageConfigService.getMessageByCode(MessageCodes.Account.RESET_PASSWORD_SUCCESS), account.getUserName(), defaultPassword));
    }

    @Override
    public ResultDto changePassThisAccount(String passwordOld, String passwordNew) {
        ResultDto result = new ResultDto();

        if (Strings.isNullOrEmpty(passwordOld)) {
            result.setResultCode(ResultDto.ResultCode.FAIL);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.PASSWORD_OLD_INVALID));
            return result;
        }
        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountDetail.getAccount();
        String pwdHashInDb = account.getPassword();
        if (!pwdHashInDb.equals(DigestUtils.md5Hex(passwordOld))) {
            result.setResultCode(ResultDto.ResultCode.FAIL);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.PASSWORD_OLD_WRONG));
            return result;
        }

        if (Strings.isNullOrEmpty(passwordNew)) {
            result.setResultCode(ResultDto.ResultCode.FAIL);
            result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.PASSWORD_OLD_INVALID));
            return result;
        }

        account.setPassword(passwordNew);
        createOrUpdate(account);

        result.setResultCode(ResultDto.ResultCode.SUCCESS);
        result.setMessage(messageConfigService.getMessageByCode(MessageCodes.Account.CHANGED_PASSWORD_SUCCESS));
        return result;
    }

    @Override
    public void logout() {
    }

    @Override
    public void updateAccount(Account account) {
        accountRepo.save(account);
    }

    @Override
    public void createOrUpdate(Account account) {
        accountRepo.save(account);
    }

    @Override
    public void createOrUpdateAccountCustomer(Account account) {

    }

    @Override
    public Account getAccountByUsername(String userName) {
        return accountRepo.getAccountByUsername(userName);
    }

    @Override
    public Account findFirstByUserName(String username) {
        return accountRepo.findFirstByUserName(username);
    }

    @Override
    public Account findFirstByUserNameAndStatus(String username, AccountStatus status) {
        return accountRepo.findFirstByUserNameAndStatus(username, status);
    }

    @Override
    public Account findFirstById(Integer id) {
        return accountRepo.findFirstById(id);
    }
}
