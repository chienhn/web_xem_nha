package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDataSelectDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDto;
import vn.dev.xem_nha.api_core.dto.banner.BannerHomeDto;
import vn.dev.xem_nha.api_core.entity.Bank;

import java.util.Date;
import java.util.List;

public interface BankService {

    void save(Bank bank);

    boolean checkNumberBank(String numberBank);
    boolean checkNumberBankUpdate(String numberBank, Long id);

    Bank getDetail(Long id);
    void delete(Long id);

    PageDto<BankDto> getPage(Date fromDate, Date toDate, String bankName, Pageable pageable);

    List<BankDataSelectDto> dataSelectAll(Long id);
}
