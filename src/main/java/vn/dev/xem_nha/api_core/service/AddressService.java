package vn.dev.xem_nha.api_core.service;

import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto;
import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto1;

import java.util.List;

public interface AddressService {
    List<AddressDto> getDataCity();

    List<AddressDto> dataSelectOfProvinces(String parentCode);

    List<AddressDto1> dataSelectOfWards(String parentCode);

}
