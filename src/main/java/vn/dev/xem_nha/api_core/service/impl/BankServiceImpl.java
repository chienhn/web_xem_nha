package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDataSelectDto;
import vn.dev.xem_nha.api_core.dto.bank.BankDto;
import vn.dev.xem_nha.api_core.entity.Bank;
import vn.dev.xem_nha.api_core.repository.BankRepo;
import vn.dev.xem_nha.api_core.service.BankService;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class BankServiceImpl implements BankService {
    @Autowired
    BankRepo bankRepo;

    @Override
    public void save(Bank bank) {

        bankRepo.save(bank);
    }

    @Override
    public boolean checkNumberBank(String numberBank) {
        return bankRepo.checkNumberBank(numberBank) != 0;
    }

    @Override
    public boolean checkNumberBankUpdate(String numberBank, Long id) {
        return bankRepo.checkNumberBankUpdate(numberBank, id) != 0;
    }

    @Override
    public Bank getDetail(Long id) {
        return bankRepo.findFirstById(id);
    }

    @Override
    public void delete(Long id) {
        bankRepo.deleteById(id);
    }

    @Override
    public PageDto<BankDto> getPage(Date fromDate, Date toDate, String bankName, Pageable pageable) {
        List<Object[]> lisBank = bankRepo.getPage(fromDate, toDate, bankName);

        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), lisBank.size());

        List<Object[]> pageObjects = lisBank.subList(start, end);
        List<BankDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:pageObjects){
            Long _id = eachObj[0] != null ? ((Long) eachObj[0]) : null;
            String _nameBank = eachObj[1] != null ? (String) eachObj[1] : null;
            String _numberBank = eachObj[2] != null ? ((String) eachObj[2]) : null;
            String _name = eachObj[3] != null ? (String) eachObj[3] : null;
            String _createdDate = null;
            try {
                if(eachObj[4] != null){
                    Date createdDate = (Date) eachObj[4] ;
                    DateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

                    _createdDate = sdf.format(createdDate);
                }
            } catch (Exception e) {}

            BankDto bankDto=new BankDto(_id, _nameBank, _numberBank, _name, _createdDate);
            listResponse.add(bankDto);
        }

        PageImpl<BankDto> pageResponse=new PageImpl<>(listResponse,pageable,lisBank.size());
        PageDto<BankDto> pageDto = PageDto.of(pageResponse,pageResponse.getContent());
        return pageDto;
    }

    @Override
    public List<BankDataSelectDto> dataSelectAll(Long id) {
        List<Map<String, Object>> all = bankRepo.getDataAll(id);

        List<BankDataSelectDto> bankDataSelectDtoArrayList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : all) {
            BankDataSelectDto bankDataSelectDto = objectMapper.convertValue(map, BankDataSelectDto.class);
            bankDataSelectDtoArrayList.add(bankDataSelectDto);
        }

        return bankDataSelectDtoArrayList;
    }
}
