package vn.dev.xem_nha.api_core.service;


import vn.dev.xem_nha.api_core.common.ServiceError;

/**
 * @Describe: Interface service xử lý logic lưu trữ cấu hình message hệ thống
 * @Author: Hoa
 * */

public interface MessageConfigService {
    ServiceError getServiceErrorByCode(Integer code);

    String getMessageByCode(Integer code);
}
