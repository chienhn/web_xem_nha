package vn.dev.xem_nha.api_core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import vn.dev.xem_nha.api_core.common.ServiceError;
import vn.dev.xem_nha.api_core.entity.MessageConfig;
import vn.dev.xem_nha.api_core.repository.MessageConfigRepo;
import vn.dev.xem_nha.api_core.service.MessageConfigService;
import org.springframework.stereotype.Service;

/**
 * @Describe: Tầng service xử lý logic lưu trữ cấu hình message hệ thống
 * @Author: Hoa
 * */

@Service
public class MessageConfigServiceImpl implements MessageConfigService {

    @Autowired
    MessageConfigRepo messageConfigRepo;

    @Override
    public ServiceError getServiceErrorByCode(Integer code) {
        ServiceError serviceError = new ServiceError(code, "");
        MessageConfig messageConfig = messageConfigRepo.findFirstByCode(code);
        if (messageConfig != null) {
            serviceError.setMessage(messageConfig.getMessage());
        }

        return serviceError;
    }

    @Override
    public String getMessageByCode(Integer code) {
        String result = "";

        MessageConfig messageConfig = messageConfigRepo.findFirstByCode(code);
        if (messageConfig != null) {
            result = messageConfig.getMessage();
        }

        return result;
    }
}
