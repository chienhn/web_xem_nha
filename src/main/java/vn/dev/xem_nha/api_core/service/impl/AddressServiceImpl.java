package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto;
import vn.dev.xem_nha.api_core.dto.addressDto.AddressDto1;
import vn.dev.xem_nha.api_core.repository.CityRepo;
import vn.dev.xem_nha.api_core.repository.DistrictsRepo;
import vn.dev.xem_nha.api_core.repository.WardsRepo;
import vn.dev.xem_nha.api_core.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service (value = "addressService")
public class AddressServiceImpl implements AddressService {

    @Resource CityRepo cityRepo;
    @Resource DistrictsRepo districtsRepo;
    @Resource WardsRepo wardsRepo;


    @Override
    public List<AddressDto> getDataCity() {
        List<Map<String, Object>> allCity = cityRepo.getDataCity();

        List<AddressDto> addressDtoList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : allCity) {
            AddressDto addressDto = objectMapper.convertValue(map, AddressDto.class);
            addressDtoList.add(addressDto);
        }

        return addressDtoList;
    }

    @Override
    public List<AddressDto> dataSelectOfProvinces(String parentCode) {
        List<Map<String, Object>> allDistrictsOfProvinces = districtsRepo.getDataSelectOfProvinces(parentCode);
        log.info("allDistrictsOfProvinces: {}", allDistrictsOfProvinces);
        List<AddressDto> districtsDataSelectDtoList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : allDistrictsOfProvinces) {
            AddressDto districtsDataSelectDto = objectMapper.convertValue(map, AddressDto.class);
            districtsDataSelectDtoList.add(districtsDataSelectDto);
        }

        return districtsDataSelectDtoList;
    }

    @Override
    public List<AddressDto1> dataSelectOfWards(String parentCode) {
        List<Map<String, Object>> allDistrictsOfWards = wardsRepo.getDataSelectOfWards(parentCode);

        List<AddressDto1> districtsDataSelectDtoList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (Map<String, Object> map : allDistrictsOfWards) {
            AddressDto1 districtsDataSelectDto = objectMapper.convertValue(map, AddressDto1.class);
            districtsDataSelectDtoList.add(districtsDataSelectDto);
        }

        return districtsDataSelectDtoList;
    }
}
