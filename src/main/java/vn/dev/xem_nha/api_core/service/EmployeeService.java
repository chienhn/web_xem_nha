package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.common.ServiceResponse;
import vn.dev.xem_nha.api_core.dto.account.UserChangePassDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDetailDto;
import vn.dev.xem_nha.api_core.dto.employee.EmployeeDto;
import vn.dev.xem_nha.api_core.dto.poster.PosterDetailsEmployeeDto;
import vn.dev.xem_nha.api_core.entity.account.Employee;

import java.util.Date;

public interface EmployeeService {
    EmployeeDetailDto getDetail(Integer id);
    Employee find(Integer employeeId);
    PageDto<EmployeeDto> getList(Date fromDate, Date toDate, Integer gender, String name, Pageable pageable);

    ServiceResponse changePassword(UserChangePassDto userChangePassDto);

    PosterDetailsEmployeeDto getDetailsPDto(Long id);

    void save(Employee employee);

    Employee getById(Integer id);
}
