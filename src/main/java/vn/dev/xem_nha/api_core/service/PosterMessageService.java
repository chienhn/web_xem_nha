package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.poster.PosterMessageDto;
import vn.dev.xem_nha.api_core.entity.PosterMessage;

import java.util.Date;
import java.util.List;

public interface PosterMessageService {
    void save(PosterMessage posterMessage);
    List<PosterMessage> listAllByPosterId(Long posterId);

    PageDto<PosterMessageDto> listMessageAll(Long id, Date fromDate, Date toDate, Integer status, Long posterId, Pageable pageable);
    PageDto<PosterMessageDto> listMessageAllMember(Long id, Date fromDate, Date toDate, Integer status, Long posterId, Long employeeId, Pageable pageable);
    PosterMessage findById(Long id);

    PosterMessageDto get(Long id);

    PosterMessage getPosterMessageById(Long id);

    Integer countPosterMessageUnseen(List<Long> ids);
}
