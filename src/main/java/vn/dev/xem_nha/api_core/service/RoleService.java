package vn.dev.xem_nha.api_core.service;

import vn.dev.xem_nha.api_core.constant.Roles;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("roleService")
public class RoleService {

    private static final List<String> ipLocalAccept = Collections.synchronizedList(new ArrayList<>());

    static {
        ipLocalAccept.add("localhost");
        ipLocalAccept.add("127.0.0.1");
        ipLocalAccept.add("0:0:0:0:0:0:0:1");
        ipLocalAccept.add("[0:0:0:0:0:0:0:1]");
    }

    private final HttpServletRequest request;

    public RoleService(HttpServletRequest request) {
        this.request = request;
    }

    public String homeView(){
        return Roles.Home.VIEW;
    }

    public String accountView(){
        return Roles.System.Account.VIEW_LIST;
    }

    private String getRemoteIp() {
        String remoteIp = request.getHeader("X-FORWARDED-FOR");
        if (org.apache.commons.lang3.StringUtils.isBlank(remoteIp)) {
            return request.getRemoteAddr();
        }
        return remoteIp;
    }
}
