package vn.dev.xem_nha.api_core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.poster.PosterMessageDto;
import vn.dev.xem_nha.api_core.entity.PosterMessage;
import vn.dev.xem_nha.api_core.repository.PosterMessageRepo;
import vn.dev.xem_nha.api_core.repository.PosterRepo;
import vn.dev.xem_nha.api_core.service.PosterMessageService;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PosterMessageServiceImpl implements PosterMessageService {

    @Autowired PosterMessageRepo posterMessageRepo;
    @Autowired PosterRepo posterRepo;

    DateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    @Override
    public void save(PosterMessage posterMessage) {
        posterMessageRepo.save(posterMessage);
    }

    @Override
    public List<PosterMessage> listAllByPosterId(Long posterId) {
        return posterMessageRepo.findAllByPosterId(posterId);
    }

    @Override
    public PageDto<PosterMessageDto> listMessageAll(Long id, Date fromDate, Date toDate, Integer status, Long posterId, Pageable pageable) {
        List<Object[]> listMessage = posterMessageRepo.getListMessage(id, fromDate, toDate, status, posterId);

        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listMessage.size());

        List<Object[]> pageObjects = listMessage.subList(start, end);
        List<PosterMessageDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:pageObjects){
            Long _id = eachObj[0] != null ? ((BigInteger) eachObj[0]) .longValue() : null;
            Long _posterId = eachObj[1] != null ? ((BigInteger) eachObj[1]).longValue() : null;
            String _name = eachObj[2] != null ? (String) eachObj[2] : null;
            String _email = eachObj[3] != null ? (String) eachObj[3] : null;
            String _phone = eachObj[4] != null ? (String) eachObj[4] : null;
            String _message = eachObj[5] != null ? (String) eachObj[5] : null;
            String _startDate = null;
            try {
                if(eachObj[6] != null){
                    Date startDate = (Date) eachObj[6] ;

                    _startDate = sdf.format(startDate);
                }
            } catch (Exception e) {}
            Integer _status = eachObj[7] != null ? (Integer) eachObj[7] : null;
            String _posterName = eachObj[8] != null ? (String) eachObj[8] : null;
            PosterMessageDto discountDto=new PosterMessageDto(_id, _posterId, _name,_email, _phone, _message, _startDate, _status, _posterName);
            listResponse.add(discountDto);
        }

        PageImpl<PosterMessageDto> pageResponse=new PageImpl<>(listResponse,pageable,listMessage.size());
        PageDto<PosterMessageDto> pageDto = PageDto.of(pageResponse,pageResponse.getContent());
        return pageDto;
    }

    @Override
    public PageDto<PosterMessageDto> listMessageAllMember(Long id, Date fromDate, Date toDate, Integer status, Long posterId, Long employeeId, Pageable pageable) {
        List<Object[]> listMessage = posterMessageRepo.getListMessageMember(id, fromDate, toDate, status, posterId, employeeId);

        final int start = (int)pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), listMessage.size());

        List<Object[]> pageObjects = listMessage.subList(start, end);
        List<PosterMessageDto> listResponse=new ArrayList<>();
        for(Object[] eachObj:pageObjects){
            Long _id = eachObj[0] != null ? ((BigInteger) eachObj[0]) .longValue() : null;
            Long _posterId = eachObj[1] != null ? ((BigInteger) eachObj[1]).longValue() : null;
            String _name = eachObj[2] != null ? (String) eachObj[2] : null;
            String _email = eachObj[3] != null ? (String) eachObj[3] : null;
            String _phone = eachObj[4] != null ? (String) eachObj[4] : null;
            String _message = eachObj[5] != null ? (String) eachObj[5] : null;
            String _startDate = null;
            try {
                if(eachObj[6] != null){
                    Date startDate = (Date) eachObj[6] ;

                    _startDate = sdf.format(startDate);
                }
            } catch (Exception e) {}
            Integer _status = eachObj[7] != null ? (Integer) eachObj[7] : null;
            String _posterName = eachObj[8] != null ? (String) eachObj[8] : null;
            PosterMessageDto discountDto=new PosterMessageDto(_id, _posterId, _name,_email, _phone, _message, _startDate, _status, _posterName);
            listResponse.add(discountDto);
        }

        PageImpl<PosterMessageDto> pageResponse=new PageImpl<>(listResponse,pageable,listMessage.size());
        PageDto<PosterMessageDto> pageDto = PageDto.of(pageResponse,pageResponse.getContent());
        return pageDto;
    }

    @Override
    public PosterMessage findById(Long id) {
        return posterMessageRepo.getById(id);
    }

    @Override
    public PosterMessageDto get(Long id) {
        List<Object[]> objects = posterMessageRepo.findBrandname(id);

        Object[] eachObj = objects.get(0);
        Long _id = eachObj[0] != null ? ((BigInteger) eachObj[0]).longValue() : null;
        Long _posterId = eachObj[1] != null ? ((BigInteger) eachObj[1]).longValue() : null;
        String _name = eachObj[2] != null ? (String) eachObj[2] : null;
        String _email = eachObj[3] != null ? (String) eachObj[3] : null;
        String _phone = eachObj[4] != null ? (String) eachObj[4] : null;
        String _message = eachObj[5] != null ? (String) eachObj[5] : null;
        String _startDate = null;
        try {
            if(eachObj[6] != null){
                Date startDate = (Date) eachObj[6] ;

                _startDate = sdf.format(startDate);
            }
        } catch (Exception e) {}
        Integer _status = eachObj[7] != null ? (Integer) eachObj[7] : null;
        String _posterName = eachObj[8] != null ? (String) eachObj[8] : null;
        return new PosterMessageDto(_id, _posterId, _name,_email, _phone, _message, _startDate, _status, _posterName);
    }

    @Override
    public PosterMessage getPosterMessageById(Long id) {
        return posterMessageRepo.getPosterMessageById(id);
    }

    @Override
    public Integer countPosterMessageUnseen(List<Long> ids) {
        int count = 0;
        for (Long id : ids) {
            count += posterMessageRepo.countPosterMessageByPosterIdAndStatus(id, 0);
        }

        return count;
    }
}
