package vn.dev.xem_nha.api_core.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.MessageCodes;
import vn.dev.xem_nha.api_core.dto.ImageDto;
import vn.dev.xem_nha.api_core.dto.recharge.RechargeDto;
import vn.dev.xem_nha.api_core.entity.Recharge;
import vn.dev.xem_nha.api_core.entity.account.Employee;
import vn.dev.xem_nha.api_core.exception.CustomException;
import vn.dev.xem_nha.api_core.repository.RechargeRepo;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.EmployeeService;
import vn.dev.xem_nha.api_core.service.MessageConfigService;
import vn.dev.xem_nha.api_core.service.RechargeService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

@Service
public class RechargeServiceImpl implements RechargeService {

    @Value(value = "${upload.path-img}")
    private String uploadPath;
    @Autowired RechargeRepo rechargeRepo;
    @Autowired
    MessageConfigService messageConfigService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    ActionHistoryService actionHistoryService;

    final static String TABLE_NAME = "RECHARGE";

    @Override
    public void add(Map<String, Object> mapParams) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMM");

        ObjectMapper objectMapper = new ObjectMapper();
        Recharge recharge = objectMapper.convertValue(mapParams, Recharge.class);

        AccountDetail accountDetail = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String orderCode = "";
        LocalDateTime now = LocalDateTime.now();
        orderCode += dtf.format(now);
        String count = Integer.toString(countOrderByMonth());
        orderCode += StringUtils.leftPad(count, 6, "0");

        boolean allowCreate = checkTradingCode(recharge.getTradingCode().trim());
        if (allowCreate) {
            throw new CustomException(messageConfigService.getServiceErrorByCode(MessageCodes.Recharge.TRANSACTION_CODE_EXISTED));
        }else {
            recharge.setCode(orderCode);
            recharge.setCreatedAt(new Date());
            recharge.setStatus(0);
            recharge.setEmployeeId(accountDetail.getAccount().getEmployee().getId().longValue());
            save(recharge);
            actionHistoryService.save("Nạp tiền", "Tạo mới lệnh nạp tiền thành công", TABLE_NAME, recharge.getId(), accountDetail.getUsername());
        }
    }

    @Override
    public PageDto<RechargeDto> getPage(Date fromDate, Date toDate, Integer status, Long employeeId, Pageable pageable) {
        Page<RechargeDto> pageResponse = rechargeRepo.getPage(fromDate, toDate, status, employeeId, pageable);
        PageDto<RechargeDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());
        return pageDto;
    }

    @Override
    public PageDto<RechargeDto> getPageMana(Date fromDate, Date toDate, Integer status, Pageable pageable) {
        Page<RechargeDto> pageResponse = rechargeRepo.getPageMana(fromDate, toDate, status, pageable);
        PageDto<RechargeDto> pageDto = PageDto.of(pageResponse, pageResponse.getContent());
        return pageDto;
    }

    @Override
    public Recharge getDetail(Long id) {
        return rechargeRepo.findFirstById(id);
    }

    @Override
    public Long getSurplus(Long employeeId) {
        return rechargeRepo.findSurplus(employeeId);
    }

    @Override
    public void approve(Recharge recharge) throws ParseException {
        Recharge current = getDetail(recharge.getId());

        AccountDetail account = (AccountDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (recharge.getStatus() == 1){
            current.setStatus(1);
            save(current);
            Employee employee = employeeService.find(Math.toIntExact(current.getEmployeeId()));
            if (employee.getSurplus() == null) {
                employee.setSurplus(current.getMoney());
            }
            else {
                employee.setSurplus(employee.getSurplus() + current.getMoney());
            }
            actionHistoryService.save("Phê duyệt ", "Khớp lệnh nạp thành công", TABLE_NAME, current.getId(), account.getUsername());

        }
        else if(recharge.getStatus() == 2){
            current.setStatus(2);
            save(current);
            actionHistoryService.save("Từ chối phê duyệt", "Khớp lệnh nạp thất bại", TABLE_NAME, current.getId(), account.getUsername());
        }
    }


    @Override
    public void save(Recharge recharge) {
        rechargeRepo.save(recharge);
    }

    @Override
    public int countOrderByMonth() {
        return rechargeRepo.countOrderByMonth();
    }

    @Override
    public boolean checkTradingCode(String tradingCode) {
        return rechargeRepo.checkTradingCode(tradingCode) != 0;
    }
}
