package vn.dev.xem_nha.api_core.service;

import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.constant.AccountStatus;
import vn.dev.xem_nha.api_core.dto.ResultDto;
import vn.dev.xem_nha.api_core.dto.account.*;
import vn.dev.xem_nha.api_core.entity.account.Account;

import java.util.List;
import java.util.Map;

public interface AccountService {
    AccountLoginResponse login(AccountLoginDto userLoginDto);

    GroupPageRoleDto getPageRoles(Account currentAuth, Integer menuId);

    PageDto<AccountLstDto> getList(Integer userId, Integer pageIndex, Integer pageSize, Integer status);

    AccountDetailDto getDetail(Integer userId);

    ListPageRoleDto getListPages();

    List<GroupDto> getListGroups();

    GroupDetailDto getDetailGroup(Integer groupId);

    Map<String, Object> updateGroupForAccount(Integer accountId, String groupIdList, Integer status);


    void addGroup(GroupDetailDto groupDetailDto);

    void updateGroup(GroupDetailDto groupDetailDto);

    void deleteGroup(Integer groupId);

    void addUserToGroup(GroupDetailDto groupDetailDto);

    AccountDetail findById(Integer accountId);

    AccountDto getInfo(String accessToken, String clientId, String clientSecret);



    void changePassword(UserChangePassDto userChangePassDto);
    Map<String, Object> addByManager(AccountCreateDto accountCreateDto);

    ResultDto register(AccountRegisterDto accountCreateDto);

    ResultDto resetPassword(Integer userId);

    ResultDto changePassThisAccount(String passwordOld, String passwordNew);

    void logout();

    void updateAccount(Account account);

    void createOrUpdate(Account account);

    void createOrUpdateAccountCustomer(Account account);

    Account getAccountByUsername(String userName);
//    Object[] getAccountByUsername(String userName);

    Account findFirstByUserName(String username);
    Account findFirstByUserNameAndStatus(String username, AccountStatus status);

    Account findFirstById(Integer id);
}
