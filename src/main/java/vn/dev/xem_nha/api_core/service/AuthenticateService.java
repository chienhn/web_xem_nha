package vn.dev.xem_nha.api_core.service;

import vn.dev.xem_nha.api_core.config.security.AccountDetail;
import vn.dev.xem_nha.api_core.dto.account.AccountDto;

public interface AuthenticateService {
    AccountDetail findByAccountId(Integer accountId);
}
