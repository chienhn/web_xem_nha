package vn.dev.xem_nha.api_core.service;

import org.springframework.data.domain.Pageable;
import vn.dev.xem_nha.api_core.common.PageDto;
import vn.dev.xem_nha.api_core.dto.poster.*;
import vn.dev.xem_nha.api_core.entity.BannerHome;
import vn.dev.xem_nha.api_core.entity.Poster;

import java.util.Date;
import java.util.List;

public interface PosterService {
    void save(Poster poster);

    Poster getById(Long id);

    Poster getByIdStatus(Long id);

    PosterDetailsDto getDetails(Long id);

    PageDto<PosterDto> getPagePoster(Integer numBedRooms,
                                     Integer numBathRooms, Integer type, Integer typeProduct, Integer province,
                                     Integer district, Integer ward, Integer level, Integer levelArea, String direction, Pageable pageable);

    PageDto<PosterManagerDto> getPagePosterMana(Date fromDate, Date toDate,Integer status, Integer type, Integer typeProduct, Integer province, Pageable pageable);
    PageDto<PosterManagerDto> getPagePosterManaMember(Date fromDate, Date toDate,Integer status, Integer type, Integer typeProduct, Integer province, Long employeeId, Pageable pageable);

    List<PosterOderByTypeProductDto> getOderByTypeProduct(Integer type, Integer typeProduct);

    List<PosterOderByTypeProductDto> getDataSelect();

    List<PosterOderByAddressDto> getOderByAddress(Integer type, Integer typeProduct);

    List<PosterOderByPiceDto> getOderByPrice( Integer type, Integer typeProduct);

    List<Long> getAllIdPoster(Long empId);


    List<Poster> getListStart();


}
