package vn.dev.xem_nha.api_core.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vn.dev.xem_nha.api_core.entity.Poster;
import vn.dev.xem_nha.api_core.service.PosterService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class CheckPostExpiredSchedule {

    @Resource
    PosterService posterService;

   // @Scheduled(cron = "20 51 03 * * ?")
   @Scheduled(cron = "30 0 0 * * ?")
    public void checkPostExpiredSchedule() {
        List<Poster> list = posterService.getListStart();
        if (list.size() > 0) {
            for (Poster poster : list) {
                poster.setStatus(0);
                poster.setUpdateTime(new Date());
                poster.setUpdateBy("Hệ thống");
                posterService.save(poster);
            }
        }

    }

}
