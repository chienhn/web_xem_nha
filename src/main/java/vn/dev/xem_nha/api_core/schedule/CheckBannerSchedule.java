package vn.dev.xem_nha.api_core.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vn.dev.xem_nha.api_core.entity.BannerHome;
import vn.dev.xem_nha.api_core.service.ActionHistoryService;
import vn.dev.xem_nha.api_core.service.BannerHomeService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class CheckBannerSchedule {

    @Resource
    BannerHomeService bannerHomeService;
    @Autowired
    ActionHistoryService actionHistoryService;

    final static String TABLE_NAME = "BANNER_HOME";

    @Scheduled(cron = "30 0 0 * * ?")
    public void checkBannerSchedule(){
        List<BannerHome> list = bannerHomeService.getListStart();
        for (BannerHome bannerHome : list) {
            List<BannerHome> configPriceActiveList = bannerHomeService.getBannerHomeEnable(bannerHome.getStt());
            if(configPriceActiveList.size() > 0) {
                for (BannerHome bannerActive : configPriceActiveList) {
                    bannerActive.setEnable(0);
                    bannerActive.setUpdatedAt(new Date());
                    bannerActive.setUpdatedBy("Hệ thống");
                    actionHistoryService.save("Stopped", "Ngừng hoạt động Banner thành công", TABLE_NAME, bannerActive.getId(), "Hệ thống");
                    bannerHomeService.save(bannerActive);

                }
            }
            bannerHome.setEnable(2);
            actionHistoryService.save("Started", "Bắt đầu hoạt động Banner thành công", TABLE_NAME, bannerHome.getId(), "Hệ thống");
            bannerHome.setUpdatedAt(new Date());
            bannerHome.setUpdatedBy("Hệ thống");
            bannerHomeService.save(bannerHome);
        }
    }
}
