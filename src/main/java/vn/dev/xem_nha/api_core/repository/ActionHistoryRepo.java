package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.ActionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "actionHistoryRepo")
public interface ActionHistoryRepo extends JpaRepository<ActionHistory, Long> {

    @Query(value = "SELECT id, table_id, action, table_name, content, created_by, created_at " +
            "FROM action_history WHERE table_name = :tableName AND table_id = :tableId order by action_history.created_at DESC ",nativeQuery = true)
    List<ActionHistory> findAllAction(@Param("tableName") String tableName, Long tableId);
}
