package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.account.Groups;
import vn.dev.xem_nha.api_core.entity.account.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Integer> {
    @Query(value = "select * from role r where r.id in (select gpr.role_id from privilege_group_page_role  gpr where gpr.group_id = :groupId and gpr.page_id = :pageId)", nativeQuery = true)
    List<Role> findAllByGroupIdAndPageId(Integer groupId, Integer pageId);

    List<Role> findAllByGroupsIn(List<Groups> groups);
}
