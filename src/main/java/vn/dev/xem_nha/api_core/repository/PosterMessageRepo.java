package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.entity.PosterMessage;

import java.util.Date;
import java.util.List;

@Repository
public interface PosterMessageRepo extends JpaRepository<PosterMessage, Long> {
    List<PosterMessage> findAllByPosterId(Long posterId);

    @Query(value = "SELECT poster_message.id, poster_message.poster_id as posterId, poster_message.name, " +
            "poster_message.email, poster_message.phone, poster_message.message," +
            "poster_message.create_time as createdTime, poster_message.status, poster.title as posterName " +
            "FROM poster_message " +
            "LEFT JOIN poster on poster.id = poster_message.poster_id " +
            "WHERE ( :id IS NULL OR poster_message.id=:id)" +
            "AND ( :fromDate IS NULL AND :toDate IS NULL OR (poster_message.create_time BETWEEN :fromDate AND :toDate)) " +
            "AND (:status IS NULL OR poster_message.status = :status) " +
            "AND (:posterId IS NULL OR poster_message.poster_id = :posterId) " +
            "ORDER BY poster_message.create_time DESC " , nativeQuery = true)
    List<Object[]> getListMessage(@Param("id") Long id,
                                                @Param("fromDate") Date fromDate,
                                                @Param("toDate") Date toDate,
                                                @Param("status") Integer status,
                                                @Param("posterId") Long posterId);

    @Query(value = "SELECT poster_message.id, poster_message.poster_id as posterId, poster_message.name, " +
            "poster_message.email, poster_message.phone, poster_message.message," +
            "poster_message.create_time as createdTime, poster_message.status, poster.title as posterName " +
            "FROM poster_message " +
            "LEFT JOIN poster on poster.id = poster_message.poster_id " +
            "WHERE ( :id IS NULL OR poster_message.id=:id)" +
            "AND ( :fromDate IS NULL AND :toDate IS NULL OR (poster_message.create_time BETWEEN :fromDate AND :toDate)) " +
            "AND (:status IS NULL OR poster_message.status = :status) " +
            "AND (:posterId IS NULL OR poster_message.poster_id = :posterId) " +
            "AND (:employeeId IS NULL OR poster.employee_id = :employeeId) " +
            "ORDER BY poster_message.create_time DESC " , nativeQuery = true)
    List<Object[]> getListMessageMember(@Param("id") Long id,
                                                @Param("fromDate") Date fromDate,
                                                @Param("toDate") Date toDate,
                                                @Param("status") Integer status,
                                                @Param("posterId") Long posterId,
                                  @Param("employeeId") Long employeeId
                                  );

    @Query(value = "SELECT poster_message.id, poster_message.poster_id as posterId, poster_message.name, " +
            "poster_message.email, poster_message.phone, poster_message.message," +
            "poster_message.create_time as createdTime, poster_message.status, poster.title as posterName " +
            "FROM poster_message " +
            "LEFT JOIN poster on poster.id = poster_message.poster_id " +
            "WHERE ( :id IS NULL OR poster_message.id=:id)" +
            "ORDER BY poster_message.create_time DESC " , nativeQuery = true)
    List<Object[]> findBrandname(@Param("id") Long id);

    PosterMessage getPosterMessageById(Long id);

    int countPosterMessageByPosterIdAndStatus(Long posterId, Integer status);
}
