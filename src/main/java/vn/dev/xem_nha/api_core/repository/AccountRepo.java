package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.dev.xem_nha.api_core.constant.AccountStatus;
import vn.dev.xem_nha.api_core.entity.account.Account;

import java.util.List;
import java.util.Map;

public interface AccountRepo extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {

    boolean existsByIdAndPassword(Integer id, String password);


    @Query(value = "select a from Account a where a.status=1 AND a.userName = :username")
    Account findFirstByUserName(@Param("username") String username);
    Account findFirstByUserNameAndStatus(String username, AccountStatus status);

    @Query(value = "select * from account a where status = 1 AND user_name = :username ", nativeQuery = true)
    Account getAccountByUsername(@Param("username") String username);


    @Query(value = "SELECT a.id, a.user_name userName, e.full_name fullName, a.status, GROUP_CONCAT(DISTINCT pg.group_name separator ',') groupRole " +
            "FROM account a " +
            "LEFT JOIN account_group ag on a.id = ag.account_id " +
            "LEFT JOIN privilege_group pg on ag.group_id = pg.id " +
            "LEFT JOIN employee e on a.employee_id = e.id " +
            "WHERE (:status IS NULL OR a.status = :status) " +
            "AND a.status != 2 " + // status = 2 : deleted
            "GROUP BY a.id, a.user_name, e.full_name , a.status",
            nativeQuery = true)
    List<Map<String, Object>> getPage(@Param("status") Integer status);

    Account findFirstById(Integer id);
}
