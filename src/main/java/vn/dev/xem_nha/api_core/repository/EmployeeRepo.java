package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.entity.account.Employee;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author trongbv
 * @version 9/16/2021
 */

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> , JpaSpecificationExecutor<Employee> {
    @Query(value = "SELECT employee.avatar, employee.full_name as name, employee.phone_number as phone, " +
                            "employee.last_login as lastLogin, employee.email as email, employee.is_online as isOnline " +
                        "FROM employee " +
                        "WHERE employee.id = :id", nativeQuery = true)
    Map<String, Object>  getDetailPDto(@Param("id") Long id);

    Employee getEmployeeById(Integer id);

    @Query(value = "SELECT employee.id as id, employee.full_name as fullName, employee.avatar as avatar, " +
            "            employee.gender as gender, employee.birthday as birthday, employee.address as address, " +
            "            employee.email as email, employee.phone_number as phone " +
            " FROM employee  WHERE :id IS NULL OR employee.id=:id",nativeQuery = true)
    List<Object[]> detailEmployee(@Param("id") Integer id);


    @Query(value = "SELECT employee.full_name as fullName, employee.avatar as avatar, " +
            " employee.gender as gender, employee.birthday as birthday, employee.address as address, employee.id, employee.surplus " +
            "FROM employee " +
            "LEFT JOIN account ON employee.id = account.employee_id " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (employee.create_time BETWEEN :fromDate AND :toDate)) " +
            "AND (:gender IS NULL OR employee.gender = :gender) " +
            "AND (:name IS NULL OR employee.full_name LIKE %:name%) " +
            "AND account.is_admin = 0 AND employee.approval_status = 1 " +
            "ORDER BY employee.create_time DESC" , nativeQuery = true)
    List<Object[]> getList(@Param("fromDate") Date fromDate,
                           @Param("toDate") Date toDate,
                           @Param("gender") Integer gender,
                           @Param("name") String name);

    @Query(value = "SELECT m FROM Employee m WHERE m.id = :employeeId")
    Employee findFirstByEmployeeId(@Param("employeeId") Integer employeeId);

//    @Query(value = "")
//    Page<Map<String, Object>> getListEmployee();
}
