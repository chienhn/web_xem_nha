package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.entity.BannerHome;
import vn.dev.xem_nha.api_core.entity.Poster;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface PosterRepo extends JpaRepository<Poster, Long> {

    Poster getPosterById(Long id);

    @Query(value = "SELECT a FROM  Poster a WHERE a.id=:id AND a.status = 2")
    Poster getByIdStatus(Long id);

    @Query(value = "SELECT poster.id, poster.title, poster.acreage, poster.total_floor as numFloors, " +
            "poster.total_bedroom as numBedRooms, poster.total_bathroom as numBathRooms, " +
            "poster.price, poster.image_url as imageUrl, poster.zip_url as zipUrl, poster.type, poster.description, " +
            "(SELECT wards.path FROM wards WHERE wards.id = poster.ward) as address, poster.posted_time as postedTime," +
            "poster.direction, products.name as strTypeProduct, poster.year_building as yearBuilding " +
            "FROM poster " +
            "LEFT JOIN products on poster.type_product = products.id " +
            "WHERE poster.id = :id", nativeQuery = true)
    Map<String, Object> getDetails(@Param("id") Long id);

    @Query(value = "SELECT poster.id, poster.title, poster.acreage, " +
                            "poster.total_bedroom as numBedRooms, poster.total_bathroom as numBathRooms, " +
                            "poster.price, poster.image_url as imageUrl, poster.type, poster.type_product as typeProduct, poster.total_floor as numFloors, " +
                            "(SELECT wards.path FROM wards WHERE wards.id = poster.ward) as address, posted_time as postedTime," +
            "                poster.direction " +
                        "FROM poster " +
                        "WHERE (:numBedRooms IS NULL OR poster.total_bedroom = :numBedRooms) " +
                            "AND (:numBathRooms IS NULL OR poster.total_bathroom = :numBathRooms) " +
                            "AND (:type IS NULL OR poster.type = :type) " +
                            "AND (:typeProduct IS NULL OR poster.type_product = :typeProduct) " +
                            "AND (:province IS NULL OR poster.province = :province) " +
                            "AND (:district IS NULL OR poster.district = :district) " +
                            "AND (:ward IS NULL OR poster.ward = :ward) " +
                            "AND (:level IS NULL OR poster.level = :level) " +
                            "AND (:levelArea IS NULL OR poster.level_area = :levelArea) " +
                            "AND (:direction IS NULL OR poster.direction = :direction) " +
                            "AND poster.status = 2" +
            "                ORDER BY poster.create_time DESC  ", nativeQuery = true)
    List<Map<String, Object>> getListPoster(
                                            @Param("numBedRooms") Integer numBedRooms,
                                            @Param("numBathRooms") Integer numBathRooms,
                                            @Param("type") Integer type,
                                            @Param("typeProduct") Integer typeProduct,
                                            @Param("province") Integer province,
                                            @Param("district") Integer district,
                                            @Param("ward") Integer ward,
                                            @Param("level") Integer level,
                                            @Param("levelArea") Integer levelArea,
                                            @Param("direction") String direction);


    @Query(value = "SELECT poster.id, poster.title, poster.acreage, " +
            "poster.total_bedroom as numBedRooms, poster.total_bathroom as numBathRooms, poster.total_view as totalView," +
            "poster.price, poster.type, poster.type_product as typeProduct, poster.total_floor as numFloors, poster.status, " +
            "(SELECT wards.path FROM wards WHERE wards.id = poster.ward) as address, posted_time as postedTime, " +
            "(SELECT count(*) FROM poster_message WHERE poster_message.poster_id = poster.id) as totalMessage, " +
            "poster.year_building as yearBuilding, poster.province, poster.district, poster.ward, poster.description, poster.image_url as imageUrl, poster.zip_url as zipUrl, poster.direction, poster.duration as duration " +
            "FROM poster " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (poster.create_time BETWEEN :fromDate AND :toDate)) " +
            "AND (:type IS NULL OR poster.type = :type) " +
            "AND (:typeProduct IS NULL OR poster.type_product = :typeProduct) " +
            "AND (:status IS NULL OR poster.status = :status) " +
            "AND (:province IS NULL OR poster.province = :province) " +
            "ORDER BY poster.create_time DESC " , nativeQuery = true)
    List<Map<String, Object>> getListPosterMana(@Param("fromDate") Date fromDate,
                                                @Param("toDate") Date toDate,
                                                @Param("type") Integer type,
                                                @Param("typeProduct") Integer typeProduct,
                                                @Param("status") Integer status,
                                                @Param("province") Integer province);

    @Query(value = "SELECT poster.id, poster.title, poster.acreage, " +
            "poster.total_bedroom as numBedRooms, poster.total_bathroom as numBathRooms, poster.total_view as totalView," +
            "poster.price, poster.type, poster.type_product as typeProduct, poster.total_floor as numFloors, poster.status, " +
            "(SELECT wards.path FROM wards WHERE wards.id = poster.ward) as address, posted_time as postedTime, " +
            "(SELECT count(*) FROM poster_message WHERE poster_message.poster_id = poster.id) as totalMessage, " +
            "poster.year_building as yearBuilding, poster.province, poster.district, poster.ward, poster.description, poster.image_url as imageUrl, poster.zip_url as zipUrl, poster.direction, poster.duration as duration " +
            "FROM poster " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (poster.create_time BETWEEN :fromDate AND :toDate)) " +
            "AND (:type IS NULL OR poster.type = :type) " +
            "AND (:typeProduct IS NULL OR poster.type_product = :typeProduct) " +
            "AND (:status IS NULL OR poster.status = :status) " +
            "AND (:province IS NULL OR poster.province = :province) " +
            "AND (:employeeId IS NULL OR poster.employee_id = :employeeId) " +
            "ORDER BY poster.create_time DESC " , nativeQuery = true)
    List<Map<String, Object>> getListPosterManaMember(@Param("fromDate") Date fromDate,
                                                @Param("toDate") Date toDate,
                                                @Param("type") Integer type,
                                                @Param("typeProduct") Integer typeProduct,
                                                @Param("status") Integer status,
                                                @Param("province") Integer province,
                                                      @Param("employeeId") Long employeeId);

    @Query(value = "SELECT a.name as typeProduct , count(b.typeProduct) as total, a.id as id FROM Poster b LEFT JOIN Products a on a.id = b.typeProduct where b.status=2" +
            "       AND :type IS NULL OR b.type = :type" +
            "       AND :typeProduct IS NULL OR b.typeProduct = :typeProduct" +
            "       group by a.name, a.id order by total desc ")

    List<Object[]> getOderByTypeProduct(@Param("type") Integer type,
                                        @Param("typeProduct") Integer typeProduct);

    @Query(value = "SELECT a.name as typeProduct, a.id as id FROM  Products a order by a.name ")
    List<Object[]> getDataSelect();

    @Query(value = "SELECT a.name as provinces , count(b.province) as total, a.id as id, a.code as code FROM Poster b LEFT JOIN Provinces a on a.id = b.province where b.status=2 " +
            "       AND :type IS NULL OR b.type = :type" +
            "       AND :typeProduct IS NULL OR b.typeProduct = :typeProduct " +
            "group by a.name, a.id, a.code order by total desc ")
    List<Object[]> getOderByAddress(@Param("type") Integer type,
                                    @Param("typeProduct") Integer typeProduct);

    @Query(value = "SELECT a.name as name , count(b.level) as total, a.id as id FROM Poster b LEFT JOIN LevelPrice a on a.id = b.level where b.status=2 " +
            "       AND :type IS NULL OR b.type = :type" +
            "       AND :typeProduct IS NULL OR b.typeProduct = :typeProduct " +
            "group by a.name, a.id order by total desc ")
    List<Object[]> getOderByPrice(@Param("type") Integer type,
                                  @Param("typeProduct") Integer typeProduct);

    @Query("SELECT b FROM Poster b WHERE b.status = 2 AND b.duration = current_date()")
    List<Poster> getListStart();




}
