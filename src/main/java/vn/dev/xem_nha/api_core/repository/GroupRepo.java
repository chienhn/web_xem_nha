package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.account.Groups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface GroupRepo extends JpaRepository<Groups, Integer>, JpaSpecificationExecutor<Groups> {
    Optional<Groups> findByGroupName(String groupName);

    Groups findFirstByGroupName(String groupName);

    boolean existsByGroupName(String groupName);

    @Query("SELECT g FROM Groups g WHERE g.id = 2")
    Groups getGroupCustomer();

//    @Query(value = "SELECT g FROM Groups g WHERE g.id IN (SELECT ag.group.id FROM AccountGroup ag WHERE ag.account.id = :accountId)")
    @Query(value = "SELECT * FROM privilege_group g WHERE g.id IN (SELECT ag.group_id FROM account_group ag WHERE ag.account_id = :accountId)", nativeQuery = true)
    List<Groups> getAllGroupByAccountId(@Param("accountId") Integer accountId);
}
