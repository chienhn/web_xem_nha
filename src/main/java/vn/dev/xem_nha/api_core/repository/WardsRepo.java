package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.address.Wards;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository(value = "wardsRepo")
public interface WardsRepo extends JpaRepository<Wards, Long> {
    @Query(value = "SELECT id, name, path_with_type as pathType FROM wards WHERE parent_code = :parentCode" +
            "       ORDER BY name ASC ",nativeQuery = true)
    List<Map<String, Object>> getDataSelectOfWards(@Param("parentCode") String parentCode);
}
