package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import vn.dev.xem_nha.api_core.entity.account.AccountGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountGroupRepo extends JpaRepository<AccountGroup, Integer> {

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM account_group WHERE account_group.id = :id", nativeQuery = true)
    void deleteAcc(@Param("id") Integer id);
}
