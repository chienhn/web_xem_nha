package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.dto.recharge.RechargeDto;
import vn.dev.xem_nha.api_core.entity.Bank;
import vn.dev.xem_nha.api_core.entity.Recharge;

import java.math.BigInteger;
import java.util.Date;

@Repository
public interface RechargeRepo extends JpaRepository<Recharge, Long> {

    @Query(value = "SELECT COUNT(created_at) " +
            "FROM recharge " +
            "WHERE (MONTH(created_at) = MONTH(LOCALTIME) AND YEAR(created_at) = YEAR(LOCALTIME)) " +
            "ORDER BY created_at"
            , nativeQuery = true
    )
    Integer countOrderByMonth();

    @Query(value = "SELECT count(b.tradingCode) FROM Recharge b WHERE b.tradingCode = :tradingCode")
    Integer checkTradingCode( @Param("tradingCode")String tradingCode );

    @Query(value = "SELECT new vn.dev.xem_nha.api_core.dto.recharge.RechargeDto(r.id, r.code, r.contentRecharge,e.fullName, r.money, r.status, " +
            "r.createdAt, b.nameBank, b.numberBank, b.name) " +
            "FROM Recharge r " +
            "LEFT JOIN Bank b ON r.bankId = b.id " +
            "LEFT JOIN Employee e ON r.employeeId = e.id " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (r.createdAt BETWEEN :fromDate AND :toDate)) " +
            "AND (:status IS NULL OR r.status = :status) " +
            "AND (:employeeId IS NULL OR r.employeeId = :employeeId) " +
            "ORDER BY r.createdAt DESC "
    )
    Page<RechargeDto> getPage(
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("status") Integer status,
            @Param("employeeId") Long employeeId,
            Pageable pageable
    );

    @Query(value = "SELECT new vn.dev.xem_nha.api_core.dto.recharge.RechargeDto(r.id, r.code, r.contentRecharge, e.fullName, r.money, r.status, " +
            "r.createdAt, b.nameBank, b.numberBank, b.name) " +
            "FROM Recharge r " +
            "LEFT JOIN Bank b ON r.bankId = b.id " +
            "LEFT JOIN Employee e ON r.employeeId = e.id " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (r.createdAt BETWEEN :fromDate AND :toDate)) " +
            "AND (:status IS NULL OR r.status = :status) " +
            "ORDER BY r.createdAt DESC "
    )
    Page<RechargeDto> getPageMana(
            @Param("fromDate") Date fromDate,
            @Param("toDate") Date toDate,
            @Param("status") Integer status,
            Pageable pageable
    );

    Recharge findFirstById(Long id);

    @Query(value = "SELECT sum(money) FROM Recharge WHERE status = 1 AND (:employeeId IS NULL OR employeeId = :employeeId)")
    Long findSurplus(@Param("employeeId") Long employeeId);
}
