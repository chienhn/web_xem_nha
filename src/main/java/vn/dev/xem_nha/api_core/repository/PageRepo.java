package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.dev.xem_nha.api_core.entity.account.Page;

import java.util.List;

public interface PageRepo extends JpaRepository<Page, Integer> , JpaSpecificationExecutor<Page> {

    @Query(value = "select * from page p where p.id in (select gpr.page_id from privilege_group_page_role gpr where gpr.group_id = :groupId )", nativeQuery = true)
    List<Page> findAllByGroupId(@Param("groupId") Integer groupId);

}
