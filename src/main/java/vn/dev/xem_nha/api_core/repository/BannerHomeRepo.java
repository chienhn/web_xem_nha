package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.entity.BannerHome;

import java.util.Date;
import java.util.List;

@Repository
public interface BannerHomeRepo extends JpaRepository<BannerHome, Long> {
    @Query(value = "SELECT banner_home.id as id, banner_home.image_url as imageUrl, banner_home.enable as enable, " +
            "banner_home.create_time as createdAt, banner_home.update_time as updatedAt, banner_home.create_by as createdBy," +
            "banner_home.update_by as updatedBy, banner_home.order_no as orderNo, banner_home.date_apply as applyDate " +
            "FROM banner_home " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (banner_home.date_apply BETWEEN :fromDate AND :toDate)) " +
            "AND (:enable IS NULL OR banner_home.enable = :enable) " +
            "ORDER BY banner_home.enable DESC, banner_home.order_no ASC " , nativeQuery = true)
    List<Object[]> getList(@Param("fromDate") Date fromDate,
                           @Param("toDate") Date toDate,
                           @Param("enable") Integer enable);


    @Query(value = "SELECT banner_home.image_url, banner_home.order_no FROM banner_home WHERE banner_home.enable = 2 ORDER BY banner_home.order_no ASC ", nativeQuery = true)
    List<Object[]> slideShow();

    @Query("SELECT b FROM BannerHome b WHERE b.enable = 2 AND b.stt = :stt")
    List<BannerHome> getBannerHomeEnable(@Param("stt") Integer stt);

    @Query("SELECT b FROM BannerHome b WHERE b.enable = 1 AND b.dateApply = current_date()")
    List<BannerHome> getListBannerHomeStart();

    @Query(value = "SELECT banner_home.id as id, banner_home.image_url as imageUrl, banner_home.enable as enable, " +
            "            banner_home.create_time as createdAt, banner_home.update_time as updatedAt, banner_home.create_by as createdBy, " +
            "            banner_home.update_by as updatedBy, banner_home.order_no as orderNo, banner_home.date_apply as applyDate" +
            " FROM banner_home  WHERE :id IS NULL OR banner_home.id=:id",nativeQuery = true)
    List<Object[]> detailBanner(@Param("id") Long id);
}
