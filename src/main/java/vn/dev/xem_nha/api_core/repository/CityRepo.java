package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.address.Provinces;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository(value = "CityRepo")
public interface CityRepo extends JpaRepository<Provinces, Long>, JpaSpecificationExecutor<Provinces> {
    @Query(value = "SELECT provinces.id, provinces.code, provinces.name " +
            "       FROM provinces" +
            "       ORDER BY provinces.name ASC ", nativeQuery = true)
    List<Map<String, Object>> getDataCity();
}
