package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.account.GroupPageRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupPageRoleRepo extends JpaRepository<GroupPageRole, Integer> {
    void deleteAllByGroupId(Integer groupId);
}
