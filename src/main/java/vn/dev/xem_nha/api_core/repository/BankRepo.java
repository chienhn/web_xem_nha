package vn.dev.xem_nha.api_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.dto.bank.BankDto;
import vn.dev.xem_nha.api_core.entity.Bank;
import vn.dev.xem_nha.api_core.entity.BannerHome;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface BankRepo extends JpaRepository<Bank, Long> {

    @Query(value = "SELECT b.id, b.nameBank, b.numberBank, b.name, b.createdAt " +
            "FROM Bank b " +
            "WHERE ( :fromDate IS NULL AND :toDate IS NULL OR (b.createdAt BETWEEN :fromDate AND :toDate)) " +
            "AND (:bankName IS NULL OR b.nameBank LIKE %:bankName%)"
    )
    List<Object[]> getPage(@Param("fromDate") Date fromDate,
                          @Param("toDate") Date toDate,
                          @Param("bankName") String bankName);

    Bank findFirstById(Long id);

    @Query(value = "SELECT count(b.numberBank) " +
            "FROM Bank b " +
            "WHERE b.numberBank = :numberBank"
    )
    Integer checkNumberBank(
            @Param("numberBank")String numberBank
    );

    @Query(value = "SELECT count(b.numberBank), count(b.id) " +
            "FROM Bank b " +
            "WHERE b.numberBank = :numberBank AND b.id=:id"
    )
    Integer checkNumberBankUpdate(
            @Param("numberBank")String numberBank,
            @Param("id")Long id
    );

    @Query(value = "SELECT b.id as id, b.nameBank as nameBank, b.numberBank as numberBank, b.name as name " +
            "FROM Bank b " +
            "WHERE :id IS NULL OR b.id = :id " +
            "ORDER BY b.nameBank ASC "

    )
    List<Map<String, Object>> getDataAll(@Param("id") Long id);
}
