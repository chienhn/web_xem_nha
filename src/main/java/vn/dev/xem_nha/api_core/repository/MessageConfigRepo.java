package vn.dev.xem_nha.api_core.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.dev.xem_nha.api_core.entity.MessageConfig;


@Repository
public interface MessageConfigRepo extends JpaRepository<MessageConfig, Integer> {

    @Query("SELECT b FROM MessageConfig b where b.code = :code")
    MessageConfig findFirstByCode(@Param("code") Integer code);
}
