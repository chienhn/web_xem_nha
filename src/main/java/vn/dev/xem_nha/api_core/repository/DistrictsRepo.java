package vn.dev.xem_nha.api_core.repository;

import vn.dev.xem_nha.api_core.entity.address.Districts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository(value = "districtsRepo")
public interface DistrictsRepo extends JpaRepository<Districts, Long> {
    @Query(value = "SELECT districts.id, districts.name, districts.code " +
            "       FROM districts " +
            "       WHERE districts.parent_code = :parentCode " +
            "       ORDER BY districts.name ASC ",nativeQuery = true)
    List<Map<String, Object>> getDataSelectOfProvinces(@Param("parentCode") String parentCode);
}
