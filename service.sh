#!/bin/bash
grepParams="XEM_NHA_API"

runPath=$grepParams
logPath=/home/...
today=process$(date '+%Y-%m-%d')
r=$(tput setaf 1)
r1=$(tput setaf 2)
N=$(tput sgr0)
env=$2

hello() {
  echo $env
}

start() {
  t1=$(ps -ef | grep $grepParams | grep -v 'grep' | wc -l)
  if [ $t1 -eq 0 ]; then
    #nohup	 java -cp :./lib/*:$grepParams vn.longth.controller.Webservice &
    #nohup	 java -Xms512m -Xmx2048m -jar Alo1030WS-1.0.jar $grepParams &
    #-Xms512m -Xmx2048m
    /usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar -Dspring.profiles.active=$env target/api_core-0.0.1-SNAPSHOT.jar $grepParams &
    # $runPath/run.sh &
    echo $grepParams "start${r1}[ok]${N}"
  fi
  if [ $t1 -eq 1 ]; then
    echo $grepParams "${r1}Started${N}"
  fi
  if (($t1 > 1)); then
    echo $grepParams "${r1}Started${N} more once instance"
  fi
}

stop() {
  /bin/ps -ef | grep $grepParams | grep -v 'grep' | awk '{print$2}' | xargs kill >/dev/null 2>&1 &
  t2=$(ps -ef | grep $grepParams | grep -v 'grep' | wc -l)
  if [ $t2 -eq 0 ]; then
    echo $grepParams "stop${r1}[ok]${N}"
  fi
  if (($t2 > 0)); then
    STOPTIMEOUT=4
    while [ $STOPTIMEOUT -gt 0 ]; do
      sleep 1
      let STOPTIMEOUT=${STOPTIMEOUT}-1
    done
    if [ $STOPTIMEOUT -eq 0 ]; then
      t3=$(ps -ef | grep $grepParams | grep -v 'grep' | wc -l)
      if (($t3 > 0)); then
        echo $grepParams "stop${r}[fail]${N}"
        echo "Timeout error when try to stop "$runPath", process is still runing..."
        echo "Use shell sh file to kill -9 program" $runPath
      fi
      if [ $t3 -eq 0 ]; then
        echo $grepParams "stop${r1}[ok]${N}"
      fi
    fi
  fi
}

kill() {
  /bin/ps -ef | grep $grepParams | grep -v 'grep' | awk '{print$2}' | xargs kill -9 >/dev/null 2>&1 &
  echo "Program "$runPath" is killed -9"
  echo $grepParams "kill -9 ${r1}[ok]${N}"
}

restart() {
  #stop
	kill
  sleep 2
  start
}

build() {
  JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 mvn clean install -DskipTests
}

case "$1" in
start)
  start
  ;;
hello)
  # shellcheck disable=SC2119
  hello
  ;;
stop)
  stop
  ;;
lesslog)
  lesslog
  ;;
status)
  t3=$(ps -ef | grep $grepParams | grep -v 'grep' | wc -l)
  if [ $t3 -eq 1 ]; then
    echo $runPath "${r1}started${N}"
  fi
  if [ $t3 -eq 0 ]; then
    echo $runPath stopped
  fi
  if (($t3 > 1)); then
    echo $runPath "processing more once instance, please kill some one..."
  fi
  ;;
taillog)
  taillog
  ;;
restart)
  restart
  ;;
kill)
  kill
  ;;
build)
  build
  ;;
*)
  echo $"Usage: $0 {start|stop|status|restart|kill|taillog|lesslog|build|backup}"
  exit 1
  ;;
esac
exit $?
